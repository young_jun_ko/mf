#Config
CXX = g++
MEX = mex -largeArrayDims -cxx



REPODIR = $(HOME)/repos/mf
DEVDIR  = $(HOME)/dev

#Libraries:
ARMADIR = $(DEVDIR)/armadillo-4.000.3
TBBDIR  = /lapmal/project/lib/tbb_41_bin/tbb41_20130314oss
# /home/junko/dev/acml-5.3.1/gfortran64_int64/lib/libcblas_acml_int64_static.a
ACMLDIR = $(DEVDIR)/acml-5.3.1/gfortran64_int64
GLOGDIR = $(DEVDIR)/glog-0.3.3/build
MATDIR = /usr/local/matlab8/extern/



MYINC = -I$(REPODIR) \
		-I$(ARMADIR)/include \
		-I$(TBBDIR)/include  \
		-I$(GLOGDIR)/include \
		-I$(MATDIR)/include

MYLD = -L$(TBBDIR)/lib/intel64/gcc4.4 \
	   -L$(GLOGDIR)/lib \
	   -L$(ACMLDIR)/lib \

MYLIBS = -lm -ltbbmalloc -ltbb  -lglog -lcblas_acml_int64_static -lacml_static


MF_SRCS = $(REPODIR)/src/MVGauss.cc

MF_OBJS = $(patsubst %.cc, %.o, $(MF_SRCS))  


#Implicit rule for cc: 
#$(CXX) -c $(CPPFLAGS) $(CXXFLAGS)

CPPFLAGS = $(MYINC) -fPIC



ssu: $(MF_OBJS) $(REPODIR)/src/mexSSU.o
	$(MEX) -o $@.mexa64 $^ $(MYLD) $(MYLIBS) 





