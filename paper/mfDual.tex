\documentclass{article} % For LaTeX2e
\usepackage{nips14submit_e,times}
\usepackage{hyperref}
\usepackage{url}
% For figures
\usepackage{graphicx} % more modern
\usepackage{subfigure} 

% For citations
%\usepackage{natbib}

\newcommand{\theHalgorithm}{\arabic{algorithm}}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amsopn}
\usepackage{ifthen}
% --- Macros ---
\input{mymacros.tex} % Standard macros
\input{mymacros2.tex}


%\documentstyle[nips13submit_09,times,art10]{article} % For LaTeX 2.09


\title{Learning Latent Factors under Non-Conjugate and Coupled Observation Models}


\author{
David S.~Hippocampus \thanks{Use footnote for providing further information
about author (webpage, alternative address)---\emph{not} for acknowledging
funding agencies.} \\
Department of Computer Science\\
Cranberry-Lemon University\\
Pittsburgh, PA 15213 \\
\texttt{hippo@cs.cranberry-lemon.edu} \\
\And
Coauthor \\
Affiliation \\
Address \\
\texttt{email} \\
}

% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\newcommand{\mc}[1]{\mathcal{#1}}

\newcommand{\st}{\sigma^2}
\newcommand{\smt}{\sigma^{-2}}
\newcommand{\Sig}[1]{\mxsigma{#1}}
%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}

\maketitle

\begin{abstract}
\end{abstract}

\section{Introduction}
\subsection{Some Notation}
We are in the usual collaborative filtering setting with a set $\mc{U}$ of $N$ users indexed by $n$ and a set $\mc{I}$ of $M$ items indexed by $m$.
The latent utility and observation models are summarized by the following latent Gaussian generative model:
\begin{align}
    \vu{n} &\sim N(0, \sigma^2 \mxi{})\\
    \vy{n} &\sim P(\vy{n} \vert \vz{n}) \quad \text{with utilities} \quad \vz{n} = \mxb{n} \mxv{}\vu{n}
    \label{eq:genmodel}
\end{align}

In the simplest case the coupling matrix $\mxb{n}$ consists of rows of the identity, but can also be used to represent couplings between items. The likelihood potentials $P(\vy{n} \vert \vz{n})$ can be non-Gaussian. 

For $\mxb{n} = \mxi{\mathcal{O}_n}$ with $\mathcal{O}_n \subseteq \mc{I}$ and $P(\vy{n} \vert \vz{n}) = N(\vy{n} \vert \vz{n}, \sigma^2_l \mxi{})$ this is probabilistic PCA.

As running examples we use the following instances of model~\ref{eq:genmodel}:
\begin{enumerate}
    \item \textbf{Count Data.} For particular items, such as songs, it is easy to capture the number of times the item was consumed as an implicit expression of user preference. Such data can be modeled by setting $\mxb{n} = \mxi{\mathcal{O}_n}$ and defining the likelihood to be Poisson.
    \item \textbf{Pairwise Preferences.} A natural and less intrusive way to query user preferences is to ask users to choose from a pair of two alternatives $(m_1, m_2) \in \mc{I}\times \mc{I}$ leading to binary preference statements wlog.\ of the form $m_1 \succ m_2$ that can be modelled using a sigmoidal likelihood such as the Logit function $\sigma(x)$\footnote{$\sigma(x) = \frac{e^x}{1+e^x}$} as $P(m_1 \succ m_2 \vert \vz{n}) = \sigma(z_{m_1} - z_{m_2})$. Let $\mc{I}^+_n, \mc{I}^-_n$ be index vectors for user n such that $\mc{I}^+_{n,i} \succ \mc{I}^-_{n,i}$. Then $\mxb{n} = \mxi{\mc{I}^+_n} -\mxi{\mc{I}^-_n}$ and $\vy{n} = \vone{}$.
\end{enumerate}

The matrix of factors $\mxv{}$ is treated as a parameter that we would like to learn using maximum marginal likelihood estimation.

\section{Learning Model Parameters}

There are several options to approach the learning problem posed as the following maximum likelihood estimation:
\begin{align}
    \max_{\mxv{}} \log P(\mxy{} \vert \mxv{}) 
    \label{eq:mlest}
\end{align}

We consider the following strategies:
\begin{enumerate}
    \item Expectation Maximization (EM)
    \item Direct lowerbound optimization
\end{enumerate}

\subsection{Expectation Maximization}

The E-Step consists of $N$ independent regression problems where an approximate Gaussian posterior $Q_n(\vu{n}) = N(\vm{n}, \Sig{n}$ is inferred. Generally, the inference method is iterative with $O(\min\{D, \vert \mc{O}_n \vert\}^3)$ per iteration.

We define the following quantities:
\begin{align}
    \mxw{n} &= \mxb{n} \mxv{}\\
    \vh{n} &= \mxw{n} \vmu{n}\\
    \vrho{n} &= \diag(\mxw{n} \Sig{n} \mxw{n})\\
    \label{eq:hrho}
\end{align}
Then the M-Step objective to minimize is:
\begin{align}
    \phi_M(\mxv{}) = \sum_n \Ex_{Q_n} \left[ - \log P(\vy{n} \vert \vu{n}, \mxv{} )\right] = \sum_n  \sum_{k \in \mc{O}_n} f_k(h_k, \rho_k; \mxv{})
    \label{eq:mobj}
\end{align}
since the $Q_n$ are considered to be fixed.  The the chain rule gives the following gradient:
\begin{align}
    \nabla_{\mxv{}} \phi_M&=\sum_n \mxb{n}^T (\sum_k \frac{\partial f_k}{\partial h_{n,k}}\vdelta{k}) \vmu{n}^T + \mxb{n}^T(\sum_k2 \frac{\partial f_k}{\partial \rho_{n,k}}\vdelta{k} \vdelta{k}^T ) \mxb{n} \mxv{}\Sig{n}
    \label{eq:gradm}
\end{align}
Thus a gradient evaluation requires $O(ND^2)$ storage for the $Q_n$ and $O(MD^2)$ to find $\vh{ }, \vrho{}$.

Instead of the $O(ND^2)$ parameterization of the posterior, one could also store the $O(\sum_n \vert \mc{O}_n \vert)$ parameterization as in (Opper, Archambeau). Then, gradient computations would require additional $2NM$ backsubstitutions for the multiplication with $\Sig{n}$ as the cholesky factorization is available for computing the log determinant.

\subsection{Direct Bound Optimization}
Another strategy is to optimize the evidence lower bound directly.
\begin{align}
    \phi_B(\mxv{}) &= \sum_n \min_{Q_n} D[ Q_n(\vu{n}) \Vert P(\vu{n}) ] + \sum_k f_k(h_k, \rho_k)\\
    &=  \sum_n \min_{\vmu{n}, \Sig{n}} \frac{1  }{2}\left[-\log \vert \Sig{n}\vert + \smt \trace(\Sig{n}) + \smt \vmu{n}^T \vmu{n}  \right] + \sum_k f_k(h_k, \rho_k)
    \label{eq:elbo}
\end{align}
At an optimal point $\{\vmu{n}^*, \Sig{n}^*\}$, the gradient is actually the same as in the M-Step: $\nabla_{\mxv{}}\phi_B = \nabla_{\mxv{}}\phi_M$, due to first order optimality conditions. This method requires an E-Step per function/gradient evaluation during which an $M\times D$ sized gradient is accumulated.


\subsection{Dual Variational Inference for the Variational Gaussian Approximation}
Following (Khan et al. 2012), the dual objective corresponding to $\phi_B$ is:
\begin{align}
    \phi_D(\mxv{}) &= - \sum_n \min_{\valpha{n}, \vlam{n}} \frac{1}{2}(\valpha{n}^T \mxw{n}\mxw{n}^T \valpha{n} - \log \vert \mxa{n}\vert) +\sum_k f^*_k(\alpha_{n,k}, \lambda_{n,k})
    \label{eq:primal}
\end{align}

At a dual minimizer  $\left( \valpha{}^*, \vlam{}^* \right)$, the gradient is:
\begin{align}
    \nabla_{\mxv{}} \phi_D &= \sum_n -\st \mxb{n}^T \valpha{n}^* \valpha{n}^{*T} \mxb{n} \mxv{} + \mxb{n}^T \diag(\vlam{n}^*) \mxb{n} \mxv{} \Sig{n}
    \label{eq:graddual}
\end{align}

Under the optimality conditions $\alpha_{k}^* =\left. \frac{\partial f_k}{\partial h_{n,k}}\right\vert_{h_{n,k}^*},\lambda_{k}^* =\left. 2\frac{\partial f_k}{\partial \rho_{n,k}}\right\vert_{\rho_{n,k}^*}$ and $\vmu{}^* = -\st \mxw{}^T \valpha{}^*$ the gradient turns out to be equivalent to  eq. (\ref{eq:gradm}). 

An optimization scheme alternating between optimizing the dual parameters and the factors (i.e.\ a dual EM algorithm) has no foundation as the dual objective is an upper bound on the  ELBO with no clear relation to the marginal likelihood. Therefore, maximizing it wrt.\ $\mxv{}$ for fixed dual parameters could even diverge as there is no clear upper bound on the dual objective.

Algorithmically, techniques to solve this kind of saddle point problems could be tried out. For example there is a very simple scheme by Arrow, Hurwizc and Uzawa to solve 
\begin{align}
    \min_{x \in \mc{X}} \max_{y\in \mc{Y}} \phi(x,y)
    \label{eq:saddle}
\end{align}
using the following iteration:
\begin{align}
    x_{t+1} &= \mc{P}_{\mc{X}}\left( x_t - \eta \nabla_x \phi(y_t, x_t) \right) \\
    y_{t+1} &= \mc{P}_{\mc{Y}}\left( y_t + \eta \nabla_y \phi(y_t, x_{t}) \right) 
    \label{algo:arrow}
\end{align}
where $\mc{P}$ is the projection operator onto the respective domain.

Also, there is an algorithm called Mirror Prox due to Nemirovski et al.
 
\cite{Yu:09}

\newpage

\bibliography{papers-short,books}
\bibliographystyle{plain}

\end{document}
