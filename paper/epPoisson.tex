\documentclass{article} % For LaTeX2e
\usepackage{nips14submit_e,times}
\usepackage{hyperref}
\usepackage{url}
% For figures
\usepackage{graphicx} % more modern
\usepackage{subfigure} 

% For citations
%\usepackage{natbib}

\newcommand{\theHalgorithm}{\arabic{algorithm}}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amsopn}
\usepackage{ifthen}
% --- Macros ---
\input{mymacros.tex} % Standard macros
\input{mymacros2.tex}


%\documentstyle[nips13submit_09,times,art10]{article} % For LaTeX 2.09


\title{Expectation Propagation for Bayesian Poisson Regression with Linear Rate Parameterization}

\author{
David S.~Hippocampus\thanks{ Use footnote for providing further information
about author (webpage, alternative address)---\emph{not} for acknowledging
funding agencies.} \\
Department of Computer Science\\
Cranberry-Lemon University\\
Pittsburgh, PA 15213 \\
\texttt{hippo@cs.cranberry-lemon.edu} \\
\And
Coauthor \\
Affiliation \\
Address \\
\texttt{email} \\
}

% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\newcommand{\mc}[1]{\mathcal{#1}}

\newcommand{\st}{\sigma^2}
\newcommand{\smt}{\sigma^{-2}}
\newcommand{\Sig}[1]{\mxsigma{#1}}
\newtheorem{proposition}{Proposition}
\newtheorem{lemma}{Lemma}
%\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}

\maketitle

\begin{abstract}
\end{abstract}

\section{Introduction}

Estimating the intensity/rate of a non-homogenous Poisson process is an important problem in many scientific- and engineering disciplines (name a few). There are several challenges that need to be tackled: often characteristics of the intensity function are known a priori. Incorporating such knowledge is crucial for reconstruction. Determining the regularization strength implied by parameters of the prior is an issue often addressed by expensive cross validation runs. 



Poisson point processes are an ubiquitous tool for modelling scientific phenomena where observations consist of counting the occurence of events of interest.
They are used to model neural activity, <Cox processes>, and image blur in photography.
In this work we will focus on a particular application from particle physics. The study of elementary particles is conducted nowadays in particle collider facilities, such as the CERN. The effects of particle collisions are recorded using specialized detectors counting the arrival of certain products of the collisions. Due to the limited resolution and accuracy of the detector, the measurements are smeared out. The task of recovering the true spectrum from these corrupted measurements is an inverse problem, referred to as unfolding. Since this raw data is the basis for further analysis, obtaining a good reconstruction is crucial for interpretation and finally scientific discoveries. Methods used in practice rely on adhoc methods for parameter seletion, and reconstructions without any measure of uncertainty of the estimates. 


- count data arises often
- poisson regression a standard tool for analysis
- many disciplines require probabilistic analysis to estimate uncertainty, principled model selection etc
- latent Gaussian/GP view on regression is a wellknown powerful framework to model different function classes
- GP poisson models intractable - MCMC slow
- in case of GP classification, EP is a defacto standard method, for approximate inference
- many people are uneasy to use approximations without knowing what is going on
- address two problems: derivation of EP updates, confidence in approximation method

- Demonstration:
    - Synthetic Data (1- or 2-D to plot things)
        - compare with MCMC results (if STAN/JAGS works well)
        - in difficult case demonstrate correction terms








\section{EP Marginal Likelihood Approximation}

Following Rasmussen, Williams

\section{Moments of Truncated Gaussians}

Denote the Gaussian pdf and cdf as $\phi(x) = N(x \vert 0,1), \Phi(x) = \int_{-\infty}^{x}\phi(t) dt$.

The main computational primitive to drive EP updates for Poisson potentials with linear rate parameterization is to be able to efficiently compute moments of a truncated Gaussian, i.e.:
\begin{align}
    \Ex\left[ s^y \vert s\geq 0 \right] &= Z_0^{-1}\int_0^{\infty}s^y N(s\vert h,\rho)ds \\ 
    Z_0 &= 1- \Phi(\frac{-h}{\sqrt{\rho}})
    \label{eq:momTruncGauss}
\end{align}

\begin{lemma}
    Consider the following quantity:
    \begin{align}
        I_r(u) = \frac{1}{1-\Phi(u)}\int_u^{\infty}x^r \phi(x) dx
        \label{eq:defI}
    \end{align}
Then the following recursive relationship holds:
    \begin{align}
        I_0(u) &= 1\\
        I_1(u) &= \frac{\phi(u)}{1-\Phi(u)}\\
        I_r(u) &=  u^{r-1}\frac{\phi(u)}{1-\Phi(u)} + (r-1) I_{r-2}(u)
        \label{eq:recI}
    \end{align}
    \label{lemma:rec}
\end{lemma}
\begin{proof}
    The base cases can be obtained directly from the definition in eq. (\ref{eq:defI}). For $r>1$ we have:
    \begin{align}
        I_r(u) &= \frac{1}{1-\Phi(u)} \int_{u}^{\infty}x^r\phi(x)dx = \frac{1}{1-\Phi(u)} \int_{u}^{\infty}x^{ r-1 }x\phi(x)dx
        \label{eq:recproof}
    \end{align}
    Applying intergration by parts we obtain the desired recursion:
    \begin{align}
        I_r(u) &= \frac{u^{r-1}\phi(u)}{1-\Phi(u)} + \frac{r-1}{1-\Phi(u)} \int_{u}^{\infty}x^{ r-2 }\phi(x)dx \\
        &= \frac{u^{r-1}\phi(u)}{1-\Phi(u)} + (r-1) I_{r-2}(u)
        \label{eq:recproof2}
    \end{align}
    This can be computed in linear time and space using dynamic programming.
\end{proof}

Next, we state the main result.
\begin{proposition}
    Let $u = \frac{-h}{\sqrt{\rho}}$. The $y$-th centered moment of a truncated Gaussian can be computed in $O(y)$ as
    \begin{align}
        \Ex\left[ s^{y} \vert s \geq 0 \right] &= \sum_{r=0}^{y} {y \choose r} h^{y-r}\rho^{ r/2 } I_r(u)
        \label{eq:mainthm}
    \end{align}
\end{proposition}
\begin{proof}
First, by definition we have
\begin{align}
    \Ex\left[ s^{y} \vert s \geq 0 \right] &= \frac{1}{1-\Phi(u)}\int_0^{\infty} s^y N(s \vert h, \rho) ds
    \label{eq:main1}
\end{align}
Next, define a mapping $g(x) = \sqrt{\rho} x + h$ and perform a change of variables $s \rightarrow x$, yielding
\begin{align}
    \frac{1}{1-\Phi(u)}\int_0^{\infty} s^y N(s \vert h, \rho) ds&= \frac{1}{1-\Phi(u)}\int_u^{\infty} (\sqrt{\rho} x + h)^y \phi(x) dx
    \label{eq:main2}
\end{align}
Expanding the polynomial $(\sqrt{\rho}x + h)^y = \sum_{r=0}^{y} {y \choose r} h^{y-r}\rho^{ r/2 } x^r$ and using linearity establishes the result:
\begin{align}
    \frac{1}{1-\Phi(u)}\int_u^{\infty} (\sqrt{\rho} x + h)^y \phi(x) dx &= \sum_{r=0}^{y} {y \choose r} h^{y-r}\rho^{ r/2 } \frac{1}{1-\Phi(u)} \int_u^{\infty} x^r \phi(x) dx  \\
    &= \sum_{r=0}^{y} {y \choose r} h^{y-r}\rho^{ r/2 } I_r(u)
    \label{eq:main3}
\end{align}
\end{proof}


\subsection{Direct Application of Integration by Parts}

Define 
\begin{align}
    I_y =\frac{1}{Z_0} \int_0^{\infty} s^y N(s \vert h,\rho) ds 
    \label{eq:altdef}
\end{align}
\begin{align}
      \Ex_Q \left[ s^y \vert 0 \leq s < \infty \right] &=  \frac{1}{Z_0}\int_0^{\infty} s^y N(s \vert h,\rho) ds \\
    &= \frac{1}{Z_0\rho} \int_0^{\infty} s^{y-1}(\rho s N(s \vert h,\rho) - h \rho N(s \vert h,\rho) ) ds + h I_{y-1}\\
    &= \frac{N(0 \vert h,\rho)}{Z_0\rho}+ \frac{y-1}{Z_0\rho} \int_0^{\infty} s^{y-2} N(s \vert h,\rho)  ds + h I_{y-1}\\
    &= \frac{N(0 \vert h,\rho)}{Z_0\rho}+ \frac{y-1}{Z_0\rho} I_{y-2} + h I_{y-1}
    \label{eq:goal}
\end{align}


\iffalse
We are interested in the following quantity:

\begin{align}
    \log Z_y =\log  \Ex_Q \left[ s^y \vert 0 \leq s < \infty \right] =\log  \int_0^{\infty} s^y N(s \vert h,\rho) ds
    \label{eq:goal}
\end{align}
and its first and second derivatives wrt. $h$.

Following the note by Pender, let
\begin{align}
    x &= \frac{-h}{\sqrt{\rho}}  \\
    a_{y,j}(h) &= {y \choose j } \sqrt{\rho^j} h^{y-j}j! \\
    b_{j,m} &= \frac{2^{-m}}{m!(j - 2m)!} && \\
    H_{j,m} (x) &= H_{j-2m-1}(x)&& \\
    x^j &= j! \sum_{m=0}^{\lfloor j/2 \rfloor} b_{j,m}H_{j+1, m}&&
    \label{eq:defs}
\end{align}
Then we have:
\begin{align}
    \log Z_y  = \log \sum_{j=0}^{y}\underbrace{ a_{y,j}(h) \left(\sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m}H_{j,m}(x) N(x \vert 0,1) \right)}_{=: f_j(h)} - \log( {1-\Phi(x)} )
    \label{eq:pender}
\end{align}
We may be able to approximate the inner sum in $f_j$ using eq. (\ref{eq:defs}) as follows:
\begin{align}
    j!\sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m}H_{j,m}(x) \approx j(j-1)! \sum_{m=0}^{\lfloor (j-1)/2 \rfloor}b_{j-1,m}H_{j,m}(x) = jx^{j-1}
    \label{eq:innerApprox}
\end{align}

The Hermite polynomials follow the following recursion:
\begin{align}
   H_0(x) &= 1 \\\quad H_1(x) &= x\\
   H_{k} &= x H_{k-1}(x) - (k-1) H_{k-2}(x)
    \label{eq:hermiteRec}
\end{align}

We study if this could lead to a recursive formulation of $\log Z_y$.
\begin{align}
    \sum_{j=0}^y f_j(h) &= a_{y,y}(h)\left(\sum_{m=0}^{\lfloor y/2 \rfloor}b_{ym}H_{y,m}(x) N(x \vert 0,1) \right) \\
    &+\sum_{j=0}^{y-1}\frac{y}{h(y-j)} a_{y-1,j}(h) N(x \vert 0,1) \sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m}  H_{j,m}(x) 
    \label{eq:rec}
\end{align}

At this point it is not clear how to proceed due to the dependencies on the sum indices $j$ and $m$. 
In the next part we therefore examine the derivatives of the moments explicitely.

\subsection{Towards the Derivatives of $\log Z_y$}
We first derive the crucial part of eq. (\ref{eq:pender}) which is $f_j$ with the aim to simplify the expressions, especially the factorial terms.
\begin{align}
    \frac{\partial x}{\partial h} &= \frac{-1}{\sqrt{\rho}} &\qquad
    \frac{\partial^2 x}{\partial h^2} &= 0 \\
    \frac{\partial a_{y,j}}{\partial h}&=\frac{y-j}{h} a_{y,j} &\qquad
    \frac{\partial^2 a_{y,j}}{\partial h^2}&=\frac{(y-j)(y-j-1)}{h^2} a_{y,j} \\
    \frac{\partial N}{\partial x} &= -xN(x|0,1)&\qquad
    \frac{\partial^2 N}{\partial x^2}&=(x^2 - 1) N(x\vert 0,1)
    \label{eq:derHelpers}
\end{align}

Furthermore, we have for $k > 1$:
\begin{align}
    \frac{\partial}{\partial x} H_k(x) &= k H_{k-1}(x) &\qquad
    \frac{\partial^2}{\partial x^2} H_k(x) &=k(k-1) H_{k-2}(x)
    \label{eq:hermitDer}
\end{align}

First derivative:
\begin{align}
    \frac{\partial f_j}{\partial h} &= \frac{\partial a_{y,j}}{\partial h}  \left(\sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m}H_{y,m}(x) N(x \vert 0,1) \right) \\
    &+ a_{y,j}(h) \left(\sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m} \frac{\partial x}{\partial h}(\frac{\partial H_{y,m}}{\partial x} N(x \vert 0,1) + H_{y,m}(x)\frac{\partial N}{\partial x}) \right)  \\
    &=  a_{y,j}(h) \sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m} \left(\frac{y - j}{h} H_{y,m}(x)+ \frac{1}{\sqrt{\rho}} H_{y+1,m}(x)\right)  N(x \vert 0,1)
    \label{eq:der1}
\end{align}

Second derivative:
\begin{align}
    \frac{\partial^2 f_j}{\partial h^2} &= \frac{\partial^2 a_{y,j}}{\partial h^2}  \left(\sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m}H_{y,m}(x) N(x \vert 0,1) \right) \\
    &+ 2\frac{\partial a_{y,j}}{\partial h}  \left(\sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m} \frac{\partial x}{\partial h}(\frac{\partial H_{y,m}}{\partial x} N(x \vert 0,1) + H_{y,m}(x)\frac{\partial N}{\partial x}) \right)  \\
    &+ a_{y,j}(h) \left(\sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m} \left(\frac{\partial x}{\partial h}\right)^2(\frac{\partial^2 H_{y,m}}{\partial x^2} N(x \vert 0,1)+2 \frac{\partial H_{y,m}}{\partial x}\frac{\partial N}{\partial x} + H_{y,m}(x)\frac{\partial^2 N}{\partial x^2}) \right)  \\
    &=  a_{y,j}(h) \sum_{m=0}^{\lfloor j/2 \rfloor}b_{j,m} \left(\frac{(y - j)(y-j-1)}{h^2} H_{y,m}(x)+ \frac{2}{\sqrt{\rho}} H_{y+1,m}(x) + \frac{1}{{\rho}} H_{y+2,m}(x)\right)  N(x \vert 0,1)
    \label{eq:der2}
\end{align}

Since for any $j$ the sum over $m$ can be constructed from $j-1$ it should be possible to compute the derivatives in $O(y)$ time and space by precomputing the necessary Hermite polynomials.
\fi
\cite{Yu:09}

\newpage

\bibliography{papers-short,books}
\bibliographystyle{plain}

\end{document}
