# Generates papers-short.bib from papers.bib by:
# - replacing elements $elem from $exchLst by "${elem}-SH"
# - replacing cross references to conference proceedings (names in $confers)
#   by vanilly entries for booktitle and year (constructed from the crossref
#   label)
# - replacing cross references of $crefName by corr. text in $crefRepl
# NOTE: In the moment, the year for the conference is determined by looking
# at the last two digits of the crossref label. If the leading digit is 5 or
# larger, we append them to "19", otherwise to "20".
# ==> Will be OK for my researcher's life!

set exchLst {ANNS ANNP ANNMS ANNM CVPR ICCV IEINF IEPAMI IENN IESP JASA JMLR JRSS-B MLEARN NCOMP NNETS JAPPL IN-MIT SIAMSC SIAMMA MRM JMRES FTML ICASSP PROCIE SIAMIS IEIP IESPM IEMI}

set confers  {UAI NIPS COLT AISTATS ICML BSTAT ECML}

# Enter the ref. for the collections/books you want to replace by short entries.
# Conference proceedings are dealt with using $confers.

set crefName(1) "Jordan:97"
set crefName(2) "Schoelkopf:98e"
set crefName(3) "Smola:99"

set crefRepl(1) "booktitle   = {Learning in Graphical Models},\n  editor      = {M.~I.~Jordan},\n  year        = {1997}"
set crefRepl(2) "booktitle   = {Advances in Kernel Methods},\n  editor      = {Sch\\\"{o}lkopf~et.~al.},\n  year        = {1998}"
set crefRepl(3) "booktitle   = {Advances in Large Margin Classifiers},\n  editor      = {Smola~et.~al.},\n  year        = {1999}"

set fp [open papers.bib r]
set text [read -nonewline $fp]
close $fp

# Substitute journal names (from $exchLst)

foreach name $exchLst {
    regsub -all -- "(= ${name})," $text {\1-SH,} text
}

# Substitute crossrefs to conf. proceedings
# Done for conf. crossrefs listed in $confers. If the crossref. is
# <name>-<no>:<yr>, where <no> is the number of the conference, <yr> the
# year (2 digits), the crossref. is replaced by:
# - booktitle: <name>-SH
# - year: 19<yr> or 20<yr>, dep. on whether <yr> < 50 or not

foreach name $confers {
    regsub -all -- "crossref\[ \]+= \{${name}-(\[0-9\]+):(\[0-4\])(\[0-9\])\}" $text "booktitle   = ${name}-SH \# \{ \\1\},\n  year        = \{20\\2\\3\}" text
    regsub -all -- "crossref\[ \]+= \{${name}-(\[0-9\]+):(\[5-9\])(\[0-9\])\}" $text "booktitle   = ${name}-SH \# \{ \\1\},\n  year        = \{19\\2\\3\}" text
}

# Explicit replacements of crossrefs (collections, books) from $crefName

foreach i [array names crefName] {
    regsub -all -- "crossref\[ \]+= \{$crefName($i)\}" $text $crefRepl($i) text
}

set fp [open papers-short.bib w]
puts $fp $text
close $fp
