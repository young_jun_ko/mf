% --------------------------------------------------------------
% Collection of macros (II)
% --------------------------------------------------------------
% Author: Matthias Seeger
% --------------------------------------------------------------
% LaTeX document format
% The following packages must be included:
% - ifthen (!)
% - amsmath
% - amsfonts
% - amsopn
% Needs mymacros.tex to be included before!
% --------------------------------------------------------------
% Last change: ??
% --------------------------------------------------------------

% Vectors
% - \v?  : normal
% - \tv? : tilde
% - \hv? : hat
% - \bv? : bar
\newcommand{\va}[2][\dummystring]{\vci[#1]{a}{#2}}
\newcommand{\vb}[2][\dummystring]{\vci[#1]{b}{#2}}
\newcommand{\vc}[2][\dummystring]{\vci[#1]{c}{#2}}
\newcommand{\vd}[2][\dummystring]{\vci[#1]{d}{#2}}
\newcommand{\ve}[2][\dummystring]{\vci[#1]{e}{#2}}
\newcommand{\vf}[2][\dummystring]{\vci[#1]{f}{#2}}
\newcommand{\vg}[2][\dummystring]{\vci[#1]{g}{#2}}
\newcommand{\vh}[2][\dummystring]{\vci[#1]{h}{#2}}
\newcommand{\vi}[2][\dummystring]{\vci[#1]{i}{#2}}
\newcommand{\vj}[2][\dummystring]{\vci[#1]{j}{#2}}
\newcommand{\vk}[2][\dummystring]{\vci[#1]{k}{#2}}
\newcommand{\vl}[2][\dummystring]{\vci[#1]{l}{#2}}
\newcommand{\vm}[2][\dummystring]{\vci[#1]{m}{#2}}
\newcommand{\vn}[2][\dummystring]{\vci[#1]{n}{#2}}
\newcommand{\vnn}[2][\dummystring]{\vci[#1]{N}{#2}}
\newcommand{\vo}[2][\dummystring]{\vci[#1]{o}{#2}}
\newcommand{\vp}[2][\dummystring]{\vci[#1]{p}{#2}}
\newcommand{\vq}[2][\dummystring]{\vci[#1]{q}{#2}}
\newcommand{\vr}[2][\dummystring]{\vci[#1]{r}{#2}}
\newcommand{\vs}[2][\dummystring]{\vci[#1]{s}{#2}}
\newcommand{\vt}[2][\dummystring]{\vci[#1]{t}{#2}}
\newcommand{\vu}[2][\dummystring]{\vci[#1]{u}{#2}}
\newcommand{\vv}[2][\dummystring]{\vci[#1]{v}{#2}}
\newcommand{\vw}[2][\dummystring]{\vci[#1]{w}{#2}}
\newcommand{\vx}[2][\dummystring]{\vci[#1]{x}{#2}}
\newcommand{\vy}[2][\dummystring]{\vci[#1]{y}{#2}}
\newcommand{\vz}[2][\dummystring]{\vci[#1]{z}{#2}}
\newcommand{\valpha}[2][\dummystring]{\vci[#1]{\alpha}{#2}}
\newcommand{\vbeta}[2][\dummystring]{\vci[#1]{\beta}{#2}}
\newcommand{\vgamma}[2][\dummystring]{\vci[#1]{\gamma}{#2}}
\newcommand{\vdelta}[2][\dummystring]{\vci[#1]{\delta}{#2}}
\newcommand{\veps}[2][\dummystring]{\vci[#1]{\eps}{#2}}
\newcommand{\vth}[2][\dummystring]{\vci[#1]{\theta}{#2}}
\newcommand{\vmu}[2][\dummystring]{\vci[#1]{\mu}{#2}}
\newcommand{\vtau}[2][\dummystring]{\vci[#1]{\tau}{#2}}
\newcommand{\vpi}[2][\dummystring]{\vci[#1]{\pi}{#2}}
\newcommand{\veta}[2][\dummystring]{\vci[#1]{\eta}{#2}}
\newcommand{\vnu}[2][\dummystring]{\vci[#1]{\nu}{#2}}
\newcommand{\vlam}[2][\dummystring]{\vci[#1]{\lambda}{#2}}
\newcommand{\vphi}[2][\dummystring]{\vci[#1]{\phi}{#2}}
\newcommand{\vvphi}[2][\dummystring]{\vci[#1]{\varphi}{#2}}
\newcommand{\vkappa}[2][\dummystring]{\vci[#1]{\kappa}{#2}}
\newcommand{\vxi}[2][\dummystring]{\vci[#1]{\xi}{#2}}
\newcommand{\vomega}[2][\dummystring]{\vci[#1]{\omega}{#2}}
\newcommand{\vsigma}[2][\dummystring]{\vci[#1]{\sigma}{#2}}
\newcommand{\vrho}[2][\dummystring]{\vci[#1]{\rho}{#2}}
\newcommand{\vpsi}[2][\dummystring]{\vci[#1]{\psi}{#2}}
\newcommand{\vzeta}[2][\dummystring]{\vci[#1]{\zeta}{#2}}

\newcommand{\tva}[2][\dummystring]{\tvci[#1]{a}{#2}}
\newcommand{\tvb}[2][\dummystring]{\tvci[#1]{b}{#2}}
\newcommand{\tvc}[2][\dummystring]{\tvci[#1]{c}{#2}}
\newcommand{\tvd}[2][\dummystring]{\tvci[#1]{d}{#2}}
\newcommand{\tve}[2][\dummystring]{\tvci[#1]{e}{#2}}
\newcommand{\tvf}[2][\dummystring]{\tvci[#1]{f}{#2}}
\newcommand{\tvg}[2][\dummystring]{\tvci[#1]{g}{#2}}
\newcommand{\tvh}[2][\dummystring]{\tvci[#1]{h}{#2}}
\newcommand{\tvi}[2][\dummystring]{\tvci[#1]{i}{#2}}
\newcommand{\tvj}[2][\dummystring]{\tvci[#1]{j}{#2}}
\newcommand{\tvk}[2][\dummystring]{\tvci[#1]{k}{#2}}
\newcommand{\tvl}[2][\dummystring]{\tvci[#1]{l}{#2}}
\newcommand{\tvm}[2][\dummystring]{\tvci[#1]{m}{#2}}
\newcommand{\tvn}[2][\dummystring]{\tvci[#1]{n}{#2}}
\newcommand{\tvnn}[2][\dummystring]{\tvci[#1]{N}{#2}}
\newcommand{\tvo}[2][\dummystring]{\tvci[#1]{o}{#2}}
\newcommand{\tvp}[2][\dummystring]{\tvci[#1]{p}{#2}}
\newcommand{\tvq}[2][\dummystring]{\tvci[#1]{q}{#2}}
\newcommand{\tvr}[2][\dummystring]{\tvci[#1]{r}{#2}}
\newcommand{\tvs}[2][\dummystring]{\tvci[#1]{s}{#2}}
\newcommand{\tvt}[2][\dummystring]{\tvci[#1]{t}{#2}}
\newcommand{\tvu}[2][\dummystring]{\tvci[#1]{u}{#2}}
\newcommand{\tvv}[2][\dummystring]{\tvci[#1]{v}{#2}}
\newcommand{\tvw}[2][\dummystring]{\tvci[#1]{w}{#2}}
\newcommand{\tvx}[2][\dummystring]{\tvci[#1]{x}{#2}}
\newcommand{\tvy}[2][\dummystring]{\tvci[#1]{y}{#2}}
\newcommand{\tvz}[2][\dummystring]{\tvci[#1]{z}{#2}}
\newcommand{\tvalpha}[2][\dummystring]{\tvci[#1]{\alpha}{#2}}
\newcommand{\tvbeta}[2][\dummystring]{\tvci[#1]{\beta}{#2}}
\newcommand{\tvgamma}[2][\dummystring]{\tvci[#1]{\gamma}{#2}}
\newcommand{\tvdelta}[2][\dummystring]{\tvci[#1]{\delta}{#2}}
\newcommand{\tveps}[2][\dummystring]{\tvci[#1]{\eps}{#2}}
\newcommand{\tvth}[2][\dummystring]{\tvci[#1]{\theta}{#2}}
\newcommand{\tvmu}[2][\dummystring]{\tvci[#1]{\mu}{#2}}
\newcommand{\tvtau}[2][\dummystring]{\tvci[#1]{\tau}{#2}}
\newcommand{\tvpi}[2][\dummystring]{\tvci[#1]{\pi}{#2}}
\newcommand{\tveta}[2][\dummystring]{\tvci[#1]{\eta}{#2}}
\newcommand{\tvnu}[2][\dummystring]{\tvci[#1]{\nu}{#2}}
\newcommand{\tvlam}[2][\dummystring]{\tvci[#1]{\lambda}{#2}}
\newcommand{\tvphi}[2][\dummystring]{\tvci[#1]{\phi}{#2}}
\newcommand{\tvvphi}[2][\dummystring]{\tvci[#1]{\varphi}{#2}}
\newcommand{\tvkappa}[2][\dummystring]{\tvci[#1]{\kappa}{#2}}
\newcommand{\tvxi}[2][\dummystring]{\tvci[#1]{\xi}{#2}}
\newcommand{\tvomega}[2][\dummystring]{\tvci[#1]{\omega}{#2}}
\newcommand{\tvsigma}[2][\dummystring]{\tvci[#1]{\sigma}{#2}}
\newcommand{\tvrho}[2][\dummystring]{\tvci[#1]{\rho}{#2}}
\newcommand{\tvpsi}[2][\dummystring]{\tvci[#1]{\psi}{#2}}
\newcommand{\tvzeta}[2][\dummystring]{\tvci[#1]{\zeta}{#2}}

\newcommand{\hva}[2][\dummystring]{\hvci[#1]{a}{#2}}
\newcommand{\hvb}[2][\dummystring]{\hvci[#1]{b}{#2}}
\newcommand{\hvc}[2][\dummystring]{\hvci[#1]{c}{#2}}
\newcommand{\hvd}[2][\dummystring]{\hvci[#1]{d}{#2}}
\newcommand{\hve}[2][\dummystring]{\hvci[#1]{e}{#2}}
\newcommand{\hvf}[2][\dummystring]{\hvci[#1]{f}{#2}}
\newcommand{\hvg}[2][\dummystring]{\hvci[#1]{g}{#2}}
\newcommand{\hvh}[2][\dummystring]{\hvci[#1]{h}{#2}}
\newcommand{\hvi}[2][\dummystring]{\hvci[#1]{i}{#2}}
\newcommand{\hvj}[2][\dummystring]{\hvci[#1]{j}{#2}}
\newcommand{\hvk}[2][\dummystring]{\hvci[#1]{k}{#2}}
\newcommand{\hvl}[2][\dummystring]{\hvci[#1]{l}{#2}}
\newcommand{\hvm}[2][\dummystring]{\hvci[#1]{m}{#2}}
\newcommand{\hvn}[2][\dummystring]{\hvci[#1]{n}{#2}}
\newcommand{\hvnn}[2][\dummystring]{\hvci[#1]{N}{#2}}
\newcommand{\hvo}[2][\dummystring]{\hvci[#1]{o}{#2}}
\newcommand{\hvp}[2][\dummystring]{\hvci[#1]{p}{#2}}
\newcommand{\hvq}[2][\dummystring]{\hvci[#1]{q}{#2}}
\newcommand{\hvr}[2][\dummystring]{\hvci[#1]{r}{#2}}
\newcommand{\hvs}[2][\dummystring]{\hvci[#1]{s}{#2}}
\newcommand{\hvt}[2][\dummystring]{\hvci[#1]{t}{#2}}
\newcommand{\hvu}[2][\dummystring]{\hvci[#1]{u}{#2}}
\newcommand{\hvv}[2][\dummystring]{\hvci[#1]{v}{#2}}
\newcommand{\hvw}[2][\dummystring]{\hvci[#1]{w}{#2}}
\newcommand{\hvx}[2][\dummystring]{\hvci[#1]{x}{#2}}
\newcommand{\hvy}[2][\dummystring]{\hvci[#1]{y}{#2}}
\newcommand{\hvz}[2][\dummystring]{\hvci[#1]{z}{#2}}
\newcommand{\hvalpha}[2][\dummystring]{\hvci[#1]{\alpha}{#2}}
\newcommand{\hvbeta}[2][\dummystring]{\hvci[#1]{\beta}{#2}}
\newcommand{\hvgamma}[2][\dummystring]{\hvci[#1]{\gamma}{#2}}
\newcommand{\hvdelta}[2][\dummystring]{\hvci[#1]{\delta}{#2}}
\newcommand{\hveps}[2][\dummystring]{\hvci[#1]{\eps}{#2}}
\newcommand{\hvth}[2][\dummystring]{\hvci[#1]{\theta}{#2}}
\newcommand{\hvmu}[2][\dummystring]{\hvci[#1]{\mu}{#2}}
\newcommand{\hvtau}[2][\dummystring]{\hvci[#1]{\tau}{#2}}
\newcommand{\hvpi}[2][\dummystring]{\hvci[#1]{\pi}{#2}}
\newcommand{\hveta}[2][\dummystring]{\hvci[#1]{\eta}{#2}}
\newcommand{\hvnu}[2][\dummystring]{\hvci[#1]{\nu}{#2}}
\newcommand{\hvlam}[2][\dummystring]{\hvci[#1]{\lambda}{#2}}
\newcommand{\hvphi}[2][\dummystring]{\hvci[#1]{\phi}{#2}}
\newcommand{\hvvphi}[2][\dummystring]{\hvci[#1]{\varphi}{#2}}
\newcommand{\hvkappa}[2][\dummystring]{\hvci[#1]{\kappa}{#2}}
\newcommand{\hvxi}[2][\dummystring]{\hvci[#1]{\xi}{#2}}
\newcommand{\hvomega}[2][\dummystring]{\hvci[#1]{\omega}{#2}}
\newcommand{\hvsigma}[2][\dummystring]{\hvci[#1]{\sigma}{#2}}
\newcommand{\hvrho}[2][\dummystring]{\hvci[#1]{\rho}{#2}}
\newcommand{\hvpsi}[2][\dummystring]{\hvci[#1]{\psi}{#2}}
\newcommand{\hvzeta}[2][\dummystring]{\hvci[#1]{\zeta}{#2}}

\newcommand{\bva}[2][\dummystring]{\bvci[#1]{a}{#2}}
\newcommand{\bvb}[2][\dummystring]{\bvci[#1]{b}{#2}}
\newcommand{\bvc}[2][\dummystring]{\bvci[#1]{c}{#2}}
\newcommand{\bvd}[2][\dummystring]{\bvci[#1]{d}{#2}}
\newcommand{\bve}[2][\dummystring]{\bvci[#1]{e}{#2}}
\newcommand{\bvf}[2][\dummystring]{\bvci[#1]{f}{#2}}
\newcommand{\bvg}[2][\dummystring]{\bvci[#1]{g}{#2}}
\newcommand{\bvh}[2][\dummystring]{\bvci[#1]{h}{#2}}
\newcommand{\bvi}[2][\dummystring]{\bvci[#1]{i}{#2}}
\newcommand{\bvj}[2][\dummystring]{\bvci[#1]{j}{#2}}
\newcommand{\bvk}[2][\dummystring]{\bvci[#1]{k}{#2}}
\newcommand{\bvl}[2][\dummystring]{\bvci[#1]{l}{#2}}
\newcommand{\bvm}[2][\dummystring]{\bvci[#1]{m}{#2}}
\newcommand{\bvn}[2][\dummystring]{\bvci[#1]{n}{#2}}
\newcommand{\bvnn}[2][\dummystring]{\bvci[#1]{N}{#2}}
\newcommand{\bvo}[2][\dummystring]{\bvci[#1]{o}{#2}}
\newcommand{\bvp}[2][\dummystring]{\bvci[#1]{p}{#2}}
\newcommand{\bvq}[2][\dummystring]{\bvci[#1]{q}{#2}}
\newcommand{\bvr}[2][\dummystring]{\bvci[#1]{r}{#2}}
\newcommand{\bvs}[2][\dummystring]{\bvci[#1]{s}{#2}}
\newcommand{\bvt}[2][\dummystring]{\bvci[#1]{t}{#2}}
\newcommand{\bvu}[2][\dummystring]{\bvci[#1]{u}{#2}}
\newcommand{\bvv}[2][\dummystring]{\bvci[#1]{v}{#2}}
\newcommand{\bvw}[2][\dummystring]{\bvci[#1]{w}{#2}}
\newcommand{\bvx}[2][\dummystring]{\bvci[#1]{x}{#2}}
\newcommand{\bvy}[2][\dummystring]{\bvci[#1]{y}{#2}}
\newcommand{\bvz}[2][\dummystring]{\bvci[#1]{z}{#2}}
\newcommand{\bvalpha}[2][\dummystring]{\bvci[#1]{\alpha}{#2}}
\newcommand{\bvbeta}[2][\dummystring]{\bvci[#1]{\beta}{#2}}
\newcommand{\bvgamma}[2][\dummystring]{\bvci[#1]{\gamma}{#2}}
\newcommand{\bvdelta}[2][\dummystring]{\bvci[#1]{\delta}{#2}}
\newcommand{\bveps}[2][\dummystring]{\bvci[#1]{\eps}{#2}}
\newcommand{\bvth}[2][\dummystring]{\bvci[#1]{\theta}{#2}}
\newcommand{\bvmu}[2][\dummystring]{\bvci[#1]{\mu}{#2}}
\newcommand{\bvtau}[2][\dummystring]{\bvci[#1]{\tau}{#2}}
\newcommand{\bvpi}[2][\dummystring]{\bvci[#1]{\pi}{#2}}
\newcommand{\bveta}[2][\dummystring]{\bvci[#1]{\eta}{#2}}
\newcommand{\bvnu}[2][\dummystring]{\bvci[#1]{\nu}{#2}}
\newcommand{\bvlam}[2][\dummystring]{\bvci[#1]{\lambda}{#2}}
\newcommand{\bvphi}[2][\dummystring]{\bvci[#1]{\phi}{#2}}
\newcommand{\bvvphi}[2][\dummystring]{\bvci[#1]{\varphi}{#2}}
\newcommand{\bvkappa}[2][\dummystring]{\bvci[#1]{\kappa}{#2}}
\newcommand{\bvxi}[2][\dummystring]{\bvci[#1]{\xi}{#2}}
\newcommand{\bvomega}[2][\dummystring]{\bvci[#1]{\omega}{#2}}
\newcommand{\bvsigma}[2][\dummystring]{\bvci[#1]{\sigma}{#2}}
\newcommand{\bvrho}[2][\dummystring]{\bvci[#1]{\rho}{#2}}
\newcommand{\bvpsi}[2][\dummystring]{\bvci[#1]{\psi}{#2}}
\newcommand{\bvzeta}[2][\dummystring]{\bvci[#1]{\zeta}{#2}}

% Matrices
% - \mx?  : normal
% - \tmx? : tilde
% - \hmx? : hat
% - \bmx? : bar
\newcommand{\mxa}[2][\dummystring]{\mx[#1]{A}{#2}}
\newcommand{\mxb}[2][\dummystring]{\mx[#1]{B}{#2}}
\newcommand{\mxc}[2][\dummystring]{\mx[#1]{C}{#2}}
\newcommand{\mxd}[2][\dummystring]{\mx[#1]{D}{#2}}
\newcommand{\mxe}[2][\dummystring]{\mx[#1]{E}{#2}}
\newcommand{\mxf}[2][\dummystring]{\mx[#1]{F}{#2}}
\newcommand{\mxg}[2][\dummystring]{\mx[#1]{G}{#2}}
\newcommand{\mxh}[2][\dummystring]{\mx[#1]{H}{#2}}
\newcommand{\mxi}[2][\dummystring]{\mx[#1]{I}{#2}}
\newcommand{\mxj}[2][\dummystring]{\mx[#1]{J}{#2}}
\newcommand{\mxk}[2][\dummystring]{\mx[#1]{K}{#2}}
\newcommand{\mxl}[2][\dummystring]{\mx[#1]{L}{#2}}
\newcommand{\mxm}[2][\dummystring]{\mx[#1]{M}{#2}}
\newcommand{\mxn}[2][\dummystring]{\mx[#1]{N}{#2}}
\newcommand{\mxo}[2][\dummystring]{\mx[#1]{O}{#2}}
\newcommand{\mxp}[2][\dummystring]{\mx[#1]{P}{#2}}
\newcommand{\mxq}[2][\dummystring]{\mx[#1]{Q}{#2}}
\newcommand{\mxr}[2][\dummystring]{\mx[#1]{R}{#2}}
\newcommand{\mxs}[2][\dummystring]{\mx[#1]{S}{#2}}
\newcommand{\mxt}[2][\dummystring]{\mx[#1]{T}{#2}}
\newcommand{\mxu}[2][\dummystring]{\mx[#1]{U}{#2}}
\newcommand{\mxv}[2][\dummystring]{\mx[#1]{V}{#2}}
\newcommand{\mxw}[2][\dummystring]{\mx[#1]{W}{#2}}
\newcommand{\mxx}[2][\dummystring]{\mx[#1]{X}{#2}}
\newcommand{\mxy}[2][\dummystring]{\mx[#1]{Y}{#2}}
\newcommand{\mxz}[2][\dummystring]{\mx[#1]{Z}{#2}}
\newcommand{\mxgamma}[2][\dummystring]{\mx[#1]{\Gamma}{#2}}
\newcommand{\mxdelta}[2][\dummystring]{\mx[#1]{\Delta}{#2}}
\newcommand{\mxth}[2][\dummystring]{\mx[#1]{\Theta}{#2}}
\newcommand{\mxsigma}[2][\dummystring]{\mx[#1]{\Sigma}{#2}}
\newcommand{\mxpi}[2][\dummystring]{\mx[#1]{\Pi}{#2}}
\newcommand{\mxlam}[2][\dummystring]{\mx[#1]{\Lambda}{#2}}
\newcommand{\mxnu}[2][\dummystring]{\mx[#1]{\nu}{#2}}
\newcommand{\mxpsi}[2][\dummystring]{\mx[#1]{\Psi}{#2}}
\newcommand{\mxphi}[2][\dummystring]{\mx[#1]{\Phi}{#2}}
\newcommand{\mxxi}[2][\dummystring]{\mx[#1]{\Xi}{#2}}

\newcommand{\tmxa}[2][\dummystring]{\tmx[#1]{A}{#2}}
\newcommand{\tmxb}[2][\dummystring]{\tmx[#1]{B}{#2}}
\newcommand{\tmxc}[2][\dummystring]{\tmx[#1]{C}{#2}}
\newcommand{\tmxd}[2][\dummystring]{\tmx[#1]{D}{#2}}
\newcommand{\tmxe}[2][\dummystring]{\tmx[#1]{E}{#2}}
\newcommand{\tmxf}[2][\dummystring]{\tmx[#1]{F}{#2}}
\newcommand{\tmxg}[2][\dummystring]{\tmx[#1]{G}{#2}}
\newcommand{\tmxh}[2][\dummystring]{\tmx[#1]{H}{#2}}
\newcommand{\tmxi}[2][\dummystring]{\tmx[#1]{I}{#2}}
\newcommand{\tmxj}[2][\dummystring]{\tmx[#1]{J}{#2}}
\newcommand{\tmxk}[2][\dummystring]{\tmx[#1]{K}{#2}}
\newcommand{\tmxl}[2][\dummystring]{\tmx[#1]{L}{#2}}
\newcommand{\tmxm}[2][\dummystring]{\tmx[#1]{M}{#2}}
\newcommand{\tmxn}[2][\dummystring]{\tmx[#1]{N}{#2}}
\newcommand{\tmxo}[2][\dummystring]{\tmx[#1]{O}{#2}}
\newcommand{\tmxp}[2][\dummystring]{\tmx[#1]{P}{#2}}
\newcommand{\tmxq}[2][\dummystring]{\tmx[#1]{Q}{#2}}
\newcommand{\tmxr}[2][\dummystring]{\tmx[#1]{R}{#2}}
\newcommand{\tmxs}[2][\dummystring]{\tmx[#1]{S}{#2}}
\newcommand{\tmxt}[2][\dummystring]{\tmx[#1]{T}{#2}}
\newcommand{\tmxu}[2][\dummystring]{\tmx[#1]{U}{#2}}
\newcommand{\tmxv}[2][\dummystring]{\tmx[#1]{V}{#2}}
\newcommand{\tmxw}[2][\dummystring]{\tmx[#1]{W}{#2}}
\newcommand{\tmxx}[2][\dummystring]{\tmx[#1]{X}{#2}}
\newcommand{\tmxy}[2][\dummystring]{\tmx[#1]{Y}{#2}}
\newcommand{\tmxz}[2][\dummystring]{\tmx[#1]{Z}{#2}}
\newcommand{\tmxgamma}[2][\dummystring]{\tmx[#1]{\Gamma}{#2}}
\newcommand{\tmxdelta}[2][\dummystring]{\tmx[#1]{\Delta}{#2}}
\newcommand{\tmxth}[2][\dummystring]{\tmx[#1]{\Theta}{#2}}
\newcommand{\tmxsigma}[2][\dummystring]{\tmx[#1]{\Sigma}{#2}}
\newcommand{\tmxpi}[2][\dummystring]{\tmx[#1]{\Pi}{#2}}
\newcommand{\tmxlam}[2][\dummystring]{\tmx[#1]{\Lambda}{#2}}
\newcommand{\tmxnu}[2][\dummystring]{\tmx[#1]{\nu}{#2}}
\newcommand{\tmxpsi}[2][\dummystring]{\tmx[#1]{\Psi}{#2}}
\newcommand{\tmxphi}[2][\dummystring]{\tmx[#1]{\Phi}{#2}}

\newcommand{\hmxa}[2][\dummystring]{\hmx[#1]{A}{#2}}
\newcommand{\hmxb}[2][\dummystring]{\hmx[#1]{B}{#2}}
\newcommand{\hmxc}[2][\dummystring]{\hmx[#1]{C}{#2}}
\newcommand{\hmxd}[2][\dummystring]{\hmx[#1]{D}{#2}}
\newcommand{\hmxe}[2][\dummystring]{\hmx[#1]{E}{#2}}
\newcommand{\hmxf}[2][\dummystring]{\hmx[#1]{F}{#2}}
\newcommand{\hmxg}[2][\dummystring]{\hmx[#1]{G}{#2}}
\newcommand{\hmxh}[2][\dummystring]{\hmx[#1]{H}{#2}}
\newcommand{\hmxi}[2][\dummystring]{\hmx[#1]{I}{#2}}
\newcommand{\hmxj}[2][\dummystring]{\hmx[#1]{J}{#2}}
\newcommand{\hmxk}[2][\dummystring]{\hmx[#1]{K}{#2}}
\newcommand{\hmxl}[2][\dummystring]{\hmx[#1]{L}{#2}}
\newcommand{\hmxm}[2][\dummystring]{\hmx[#1]{M}{#2}}
\newcommand{\hmxn}[2][\dummystring]{\hmx[#1]{N}{#2}}
\newcommand{\hmxo}[2][\dummystring]{\hmx[#1]{O}{#2}}
\newcommand{\hmxp}[2][\dummystring]{\hmx[#1]{P}{#2}}
\newcommand{\hmxq}[2][\dummystring]{\hmx[#1]{Q}{#2}}
\newcommand{\hmxr}[2][\dummystring]{\hmx[#1]{R}{#2}}
\newcommand{\hmxs}[2][\dummystring]{\hmx[#1]{S}{#2}}
\newcommand{\hmxt}[2][\dummystring]{\hmx[#1]{T}{#2}}
\newcommand{\hmxu}[2][\dummystring]{\hmx[#1]{U}{#2}}
\newcommand{\hmxv}[2][\dummystring]{\hmx[#1]{V}{#2}}
\newcommand{\hmxw}[2][\dummystring]{\hmx[#1]{W}{#2}}
\newcommand{\hmxx}[2][\dummystring]{\hmx[#1]{X}{#2}}
\newcommand{\hmxy}[2][\dummystring]{\hmx[#1]{Y}{#2}}
\newcommand{\hmxz}[2][\dummystring]{\hmx[#1]{Z}{#2}}
\newcommand{\hmxgamma}[2][\dummystring]{\hmx[#1]{\Gamma}{#2}}
\newcommand{\hmxdelta}[2][\dummystring]{\hmx[#1]{\Delta}{#2}}
\newcommand{\hmxth}[2][\dummystring]{\hmx[#1]{\Theta}{#2}}
\newcommand{\hmxsigma}[2][\dummystring]{\hmx[#1]{\Sigma}{#2}}
\newcommand{\hmxpi}[2][\dummystring]{\hmx[#1]{\Pi}{#2}}
\newcommand{\hmxlam}[2][\dummystring]{\hmx[#1]{\Lambda}{#2}}
\newcommand{\hmxnu}[2][\dummystring]{\hmx[#1]{\nu}{#2}}
\newcommand{\hmxpsi}[2][\dummystring]{\hmx[#1]{\Psi}{#2}}
\newcommand{\hmxphi}[2][\dummystring]{\hmx[#1]{\Phi}{#2}}

\newcommand{\bmxa}[2][\dummystring]{\bmx[#1]{A}{#2}}
\newcommand{\bmxb}[2][\dummystring]{\bmx[#1]{B}{#2}}
\newcommand{\bmxc}[2][\dummystring]{\bmx[#1]{C}{#2}}
\newcommand{\bmxd}[2][\dummystring]{\bmx[#1]{D}{#2}}
\newcommand{\bmxe}[2][\dummystring]{\bmx[#1]{E}{#2}}
\newcommand{\bmxf}[2][\dummystring]{\bmx[#1]{F}{#2}}
\newcommand{\bmxg}[2][\dummystring]{\bmx[#1]{G}{#2}}
\newcommand{\bmxh}[2][\dummystring]{\bmx[#1]{H}{#2}}
\newcommand{\bmxi}[2][\dummystring]{\bmx[#1]{I}{#2}}
\newcommand{\bmxj}[2][\dummystring]{\bmx[#1]{J}{#2}}
\newcommand{\bmxk}[2][\dummystring]{\bmx[#1]{K}{#2}}
\newcommand{\bmxl}[2][\dummystring]{\bmx[#1]{L}{#2}}
\newcommand{\bmxm}[2][\dummystring]{\bmx[#1]{M}{#2}}
\newcommand{\bmxn}[2][\dummystring]{\bmx[#1]{N}{#2}}
\newcommand{\bmxo}[2][\dummystring]{\bmx[#1]{O}{#2}}
\newcommand{\bmxp}[2][\dummystring]{\bmx[#1]{P}{#2}}
\newcommand{\bmxq}[2][\dummystring]{\bmx[#1]{Q}{#2}}
\newcommand{\bmxr}[2][\dummystring]{\bmx[#1]{R}{#2}}
\newcommand{\bmxs}[2][\dummystring]{\bmx[#1]{S}{#2}}
\newcommand{\bmxt}[2][\dummystring]{\bmx[#1]{T}{#2}}
\newcommand{\bmxu}[2][\dummystring]{\bmx[#1]{U}{#2}}
\newcommand{\bmxv}[2][\dummystring]{\bmx[#1]{V}{#2}}
\newcommand{\bmxw}[2][\dummystring]{\bmx[#1]{W}{#2}}
\newcommand{\bmxx}[2][\dummystring]{\bmx[#1]{X}{#2}}
\newcommand{\bmxy}[2][\dummystring]{\bmx[#1]{Y}{#2}}
\newcommand{\bmxz}[2][\dummystring]{\bmx[#1]{Z}{#2}}
\newcommand{\bmxgamma}[2][\dummystring]{\bmx[#1]{\Gamma}{#2}}
\newcommand{\bmxdelta}[2][\dummystring]{\bmx[#1]{\Delta}{#2}}
\newcommand{\bmxth}[2][\dummystring]{\bmx[#1]{\Theta}{#2}}
\newcommand{\bmxsigma}[2][\dummystring]{\bmx[#1]{\Sigma}{#2}}
\newcommand{\bmxpi}[2][\dummystring]{\bmx[#1]{\Pi}{#2}}
\newcommand{\bmxlam}[2][\dummystring]{\bmx[#1]{\Lambda}{#2}}
\newcommand{\bmxnu}[2][\dummystring]{\bmx[#1]{\nu}{#2}}
\newcommand{\bmxpsi}[2][\dummystring]{\bmx[#1]{\Psi}{#2}}
\newcommand{\bmxphi}[2][\dummystring]{\bmx[#1]{\Phi}{#2}}

% Other stuff
\newcommand{\subi}{\backslash i}
\newcommand{\subj}{\backslash j}
\newcommand{\subij}{\backslash i,j}
\newcommand{\subbgi}{\backslash I}

\newcommand{\rg}[2][1]{{#1}\dots{#2}}
\newcommand{\rng}[2][1]{{#1},\dots,{#2}}
\newcommand{\vrng}[3][1]{{#2}_{#1},\dots,{#2}_{#3}}
\newcommand{\srng}[2][1]{\{{#1},\dots,{#2}\}}
\newcommand{\svrng}[3][1]{\{{#2}_{#1},\dots,{#2}_{#3}\}}
