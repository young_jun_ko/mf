function [f, grad] = funDVI(lambda, V, y, lik, hyp)
    % Dual objective function for Variational Gaussian Approximate inference 
    %e L(lambda) = 0.5 s2u(lambda-y)' VV' (lambda-y) - 0.5 log|A| + sum(g(lambda))

    % prepare log determinant
    D = size(V,2);
    A = V' * diag(lambda) * V + eye(D) / hyp.s2u;
    L = chol(A,'lower');
    lda = 2 * sum(log(diag(L)));

    % prepare squared term
    m = V' * (lambda - y);

    % likelihood part
    [con, dcon] = cvxCon(lik,lambda);
    
    f = 0.5 *(hyp.s2u * (m' * m) - lda) + con;

    % prepare gradient
    M = (L \ V')'; %log det part:
    d = sum(M.*M,2); % diag(V*inv(A)*V')

    grad = hyp.s2u * V * m - 0.5 * d + dcon;
