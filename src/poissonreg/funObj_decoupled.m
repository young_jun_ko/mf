function [f,g,h]=funObj_decoupled(x, y, logLink, mu, Sigma, N, bound, hessianApprox)

    % objective function for decoupled variational inference 
    inds = find(x < 0);

    if(~isempty(inds))
        f = inf;
        g = 0;
        h = 0;
        fprintf('returned because nonpositive\n')
        return
    end

    lambda = x;

    % compute some quantities
    err = lambda - y;
    t1 = Sigma*err;
    A = Sigma + diag(1./lambda);
    n = size(Sigma, 1);
    iA = A\eye(n);

    L  = chol(A,'lower');
    lda = 2* sum(log(diag(L)));
    % compute parts due to the KL distance and its gradient
    kl = 0.5*(-lda -sum(log(lambda)) + err'*t1);
    grad_kl = 0.5*(diag(iA)./lambda.^2 - 1./lambda) + t1

    % compute part due to the data and its gradient
    data =  -mu'*err + lambda'*(log(lambda) - 1); 
    grad_data = -mu + log(lambda);

    % add
    f = kl + data;
    g = grad_kl + grad_data;
    if(nargout > 2)      
        h_data = diag(1./lambda);% + 0.5*diag(1./(lambda.^2)); 

        if isempty(hessianApprox); hessianApprox = 'full'; end;
        switch hessianApprox
            case 'diag'
                h_quad =  0.5*diag(1./lambda.^2) + diag(diag(Sigma)) - ...
                0.5*diag(1./lambda.^3)*diag(diag(iA));
            case 'full'
                h_quad = 0.5*diag(1./lambda.^2) + Sigma - ...
                0.5*diag(1./lambda.^3)*diag(diag(iA)); 

            otherwise
                error('no such method');
        end
        h = h_data + h_quad;
    end

