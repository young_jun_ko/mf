function [fx, grad] = poissonRegMDual(v,y,lambda, hyp)
    [M,N] = size(y);
    D = length(v) / M;

    V = reshape(v, M,D);
    alpha = lambda - y;
    sumA2 = alpha * alpha';
    fx =  0.5*hyp.s2u * trace((sumA2 * V) * V');
    grad = hyp.s2u * sumA2 * V;
    for n = 1:N
        A = V' * diag(lambda(:,n)) * V + eye(D) / hyp.s2u;
        L = chol(A,'lower');
        lda = 2*sum(log(diag(L)));
        S = L'\(L\eye(D));
        fx = fx - 0.5*lda; 
        grad = grad - (S * V' * diag(lambda(:,n)))';
    end
    grad = grad(:);
