function [f, grad] = funDVIfull(Lambda, y, lik, hyp)
    % Dual objective function for Variational Gaussian Approximate inference 
    % L(lambda) = 0.5 s2u(lambda-y)' VV' (lambda-y) - 0.5 log|A| + sum(g(lambda))
    global Vg;

    V = Vg;
    D = size(V,2);
    [M,N] = size(y);
    Lam = reshape(Lambda, M, N); 

    Alpha = Lam - y;

    v0 = V(:);
    mfopts.DerivativeCheck = 'on';

    v = minFunc(@funDualV, v0, mfopts, Lam, Alpha,hyp);
    V = reshape(v, M, D);


    grad = zeros(M,N);
    f=0;
    for n = 1:N
        lambda = Lam(:,n);
        % prepare log determinant
        A = V' * diag(lambda) * V + eye(D) / hyp.s2u;
        L = chol(A,'lower');
        lda = 2 * sum(log(diag(L)));

        % prepare squared term
        m = V' * (lambda - y);

        % likelihood part
        [fstar, dfstar] = cvxCon(lik,lambda);

        f = f + 0.5 *(hyp.s2u * (m' * m) - lda) + fstar;

        % prepare gradient
        M = (L \ V')'; %log det part:
        d = sum(M.*M,2); % diag(V*inv(A)*V')

        grad(:,n) = hyp.s2u * V * m - 0.5 * d + dfstar;
    end
    grad = grad(:);
