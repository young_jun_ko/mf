function [fx, grad] = funDualV(v, Lambda, Alpha, hyp)
    [M,N] = size(Alpha);
    D = length(v) / M;
    V = reshape(v,M,D);

    sumA2 = Alpha * Alpha';
    fx = -0.5 * hyp.s2u * trace((sumA2 * V) * V');

    grad =-hyp.s2u * sumA2 * V;

    lda = 0;
    for n = 1:N
        lambda = Lambda(:,n);
        An = V' * diag(lambda) * V + eye(D) / hyp.s2u;
        Ln = chol(An,'lower');
        lda = lda + 2*sum(log(diag(Ln)));
        Sn = Ln' \ (Ln \ eye(D));
        grad = grad +(Sn * V' * diag(lambda))'; 
    end
    fx = fx + 0.5 * lda;
    grad = grad(:);
