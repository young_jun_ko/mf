function stop = recorder(x,iterationType,i,funEvals,f,t,gtd,g,d,optCond, opts,varargin )

    global statsIter

    run = statsIter.(opts.runName);
    run.fIt(end+1) = f;
    run.xIt{end+1} = x;
    run.timeIt(end+1) = toc;

    statsIter.(opts.runName) = run;
    stop = false;
