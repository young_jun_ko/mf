function [f, grad] = funPoissonRegDVI(lambda, V, y, lik, hyp)
    % Dual objective function for Variational Gaussian Approximate inference with Poisson likelihoods 
    % L(lambda) = 0.5 s2u(lambda-y)' VV' (lambda-y) - 0.5 log|A| + sum(g(lambda))

    % prepare log determinant
    D = size(V,2);
    A = V' * diag(lambda) * V + eye(D) / hyp.s2u;
    L = chol(A,'lower');
    lda = 2 * sum(log(diag(L)));

    % prepare squared term
    m = V' * (lambda - y);

    [con, dcon] = cvxCon(lik,lambda);
    
    %f = 0.5 *(hyp.s2u * (m' * m) - lda) + sum(lambda .* (log(lambda) - 1));
    f = 0.5 *(hyp.s2u * (m' * m) - lda) + con;


    % prepare gradient
    M = (L \ V')'; d = sum(M.*M,2); %log det part 

    grad = hyp.s2u * V * m - 0.5 * d + dcon;
