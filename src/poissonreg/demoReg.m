% demo of bayesian poisson regression

M = 100;
N = 1;% later we may want to do multiple output regression
D = 2;
s2u = 1;
hyp.s2u = s2u;

lik = likPoisson();

rng(1);
% DRAW WEIGHTS
u = randn(D, N) * sqrt(hyp.s2u);

% DRAW DATA
V = rand(M, D);
%V = eye(D);
b = 0;% randn(M, 1)*3;
lambda = exp(V*u + b);
y = poissrnd(lambda);

opts.maxIter = 120;
[mu, Sigma, lambda1] = mfvb(y, V,b, hyp, opts);

lambda0 = rand(size(V,1),1);
lambda = minFunc(@funPoissonRegDVI, lambda0, [],V, y, lik,hyp);

alpha = lambda - y;
A = V' * diag(lambda) * V + eye(D) / hyp.s2u;

L = chol(A,'lower');
SigmaD = L' \ (L\eye(D));
muD = - hyp.s2u * V' * alpha;


[f, g] = funDVI(lambda, V, y, lik, hyp);

%[f1,g1]=funObj_decoupled(lambda, y, @lvb_poisson, zeros(D,1),s2u * eye(D) , N, [], []);


[fxPrim, fxDual] = funPrimalVI(V,lambda, y, lik, hyp)
