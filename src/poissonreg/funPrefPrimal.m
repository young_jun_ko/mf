function [fx,grad] = funPrefPrimal(v, y,lik, hyp, opts)
    %Objective function for Poisson multiple output regression
    % L(V) = sum_n E_Qn [ log P(yn | V, un) ] where log P(yn|V,un) = \prod_{m \in On} ymn*vm'*un - exp(vm'*un)
    %
    % INPUT:
    % - v: vector representing V in column major order, i.e. v = V(:)
    % - y: data MxN
    % - hyp.s2u: prior variance
    % - opts.maxIter: parameters for inference algorithm
    %
    % OUPUT
    % - fx: function value
    % - grad: gradient wrt. V
    [M,N] = size(y);
    D = length(v) / M;
    V = reshape(v, M, D);

    fxPrim = 0;
    %fxDual = 0;
    grad = zeros(M,D);

    for n = 1:N
        yn = y(:,n);
        [sn, Sn, lambda, lda] = mfvb(yn, V, 0, hyp, opts);

        grad = grad - yn * sn';
        %Primal objective
        h = V * sn;
        rho = sum(V .* (V *Sn),2);
        expTerm = exp(h+0.5*rho);
        f_lik = -yn'*h+sum(expTerm);
        fxPrim = fxPrim - 0.5*(lda+trace(Sn)/hyp.s2u+(sn'*sn)/hyp.s2u)-f_lik;

        for m = 1:M
            grad(m,:) = grad(m,:) + expTerm(m) * (V(m,:) * Sn + sn');
        end

        %Dual objective
        %alpha = lambda - yn;
        %Vta = V' * alpha;
        %f_star = sum(lambda.*(log(lambda)-1));
        %fxDual = fxDual + 0.5*(hyp.s2u*(Vta'*Vta)-lda-D)+f_star;
    end
    %duality gap should be 0
    %[fxDual, fxPrim]
    fx = - fxPrim;
    grad = grad(:);
