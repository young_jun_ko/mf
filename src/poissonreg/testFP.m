function [lambda,alpha, Sigma, mu] = testFP(lambda0, V, y, hyp)
    D = size(V,2);
    lambda = lambda0;
    alpha = lambda - y;
    for it=1:250
        A = V' * diag(lambda) * V + eye(D) / hyp.s2u;
        L = chol(A,'lower');
    Sigma = L' \ (L \ eye(D));
        rho = diag(V * Sigma * V');
        mu = - hyp.s2u * V' * alpha;
        h = V * mu;
        sigmo = likLogit.sigmoid(h + rho * 0.5);
        lambda =  sigmo;
        alpha = sigmo - y;
        %[it lambda']

    end
