classdef likLogit < handle
    methods
        function obj = likLogit()
        end

        % calculates the convex conjugate and its derivative
        % f*(lambda) = \max_{h, rho} alpha'*h + lambda'*rho/2 - \sum_n f_n(h_n, rho_n)
        % where f is defined by this.getF()
        function [con, dcon] = cvxCon(this, lambda)
            logLam = log(lambda);
            log1Lam = log(1-lambda);
            dcon = logLam - log1Lam;
            con = lambda' * logLam + (1-lambda)' * log1Lam;
        end

        % calculates f = E_eta [ -log P(y | eta) ]
        % with Q(eta) = N(h,R) and rho = diag(R)
        function [f] = getF(this, y, h, rho)
            f = - y' * h + sum(log1p(exp(h + rho * 0.5)));
        end
        function [f] = getFExact(this, y, h, rho)
            %calculate E_Q(eta)[ - log P(y | eta) ]
            P = length(h);
            nSamples = 10000;
            etaSamples = bsxfun(@plus, bsxfun(@times, randn(P, nSamples), sqrt(rho)), h);
            Ellp = mean(log1p(exp(etaSamples)),2);
            f = -y' * h + sum(Ellp);

        end
        function [alpha] = getAlpha(this, lambda, y)
            alpha = lambda - y; 
        end
    end

    methods(Static)
        function p = sigmoid(eta)
            p = 0.5  + 0.5 * tanh(eta*0.5);
            %p = exp(eta) ./ (1+exp(eta));
        end
    end

end
