function [f] = funOpperArch(x, V, y, lik, hyp)
    P = length(y);
    lambda = x(1:P);
    mu = x((P+1):end);
    % prepare log determinant
    D = size(V,2);
    A = V' * diag(lambda) * V + eye(D) / hyp.s2u;
    L = chol(A,'lower');
    lda = 2 * sum(log(diag(L)));
    Sigma = L' \ (L\eye(D));
    h = V * mu;
    rho = sum(V .* (V*Sigma),2);
    f_lik = getFExact(lik, y, h, rho);
    f_lik = getF(lik, y, h, rho);

    fx = - 0.5*(lda+trace(Sigma)/hyp.s2u+(mu'*mu)/hyp.s2u)-f_lik;
    f =  -fx;
