function [fx, grad] = funPoissonMultiDual(v, opts, y, hyp)
    %Objective function for Poisson multiple output regression using the dual formulation
    % L(V) = 0.5 * a'*VV'a - 0.5*logdet(A)
    %
    % INPUT:
    % - v: vector representing V in column major order, i.e. v = V(:)
    % - y: data MxN
    % - hyp.s2u: prior variance
    % - opts.maxIter: parameters for inference algorithm
    %
    % OUPUT
    % - fx: function value
    % - grad: gradient wrt. V

    [M,N] = size(y);
    D = length(v) / M;
    V = reshape(v, M, D);

    fx = 0;
    grad = zeros(M,D);

    for n = 1:N
        yn = y(:,n);
        [fn, gn] = funPoissonSingleDual(V, yn, hyp, opts);
        fx= fx + fn;
        grad = grad + gn;
    end
    grad = grad(:);
