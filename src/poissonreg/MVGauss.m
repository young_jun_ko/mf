classdef MVGauss<handle

    properties(Constant)
        Cpi = -0.5 * log(2*pi);
    end

    properties
        Sigma
        mu
        L
        ldSigma
        logPart
        dim
    end

    methods(Static)
        % Natural Paramters to Moment Parameters
        function [Sigma, mu, L , ldSigma, dim, logPart] = nat2mom(A, b)
            dim = length(b);
            L = chol(A,'lower');
            Linv = L \ eye(size(L));
            Sigma = Linv' * Linv;
            mu = L' \ (L\b);
            ldSigma = -2*sum(log(diag(L)));
            logPart = dim * MVGauss.Cpi - 0.5 * ldSigma;
        end

    end

    methods

        function obj = MVGauss(varargin)
            if nargin == 1
                D = varargin{1};
                obj.dim = D;
                obj.Sigma = zeros(D);
                obj.mu = zeros(D,1);
                obj.L = zeros(D);
                obj.ldSigma = 0;
                obj.logPart = 0;
            else
                A = varargin{1}; b = varargin{2};
                [obj.Sigma, obj.mu, obj.L, obj.ldSigma, obj.dim, obj.logPart] = MVGauss.nat2mom(A,b);
            end
        end
        % reset parameters
        function this = reset(this, A, b) 
            [this.Sigma, this.mu, this.L, this.ldSigma, this.dim, this.logPart] = MVGauss.nat2mom(A,b);
        end

        % infer state from natural likelihood params and isotropic zero mean prior
        function this = infer(this, Alh, blh, s2)
            reset(this, Alh + eye(size(Alh)) / s2, blh);
        end

        % computes KL[ this || N(0, s2*I) ]
        function kl = getKLIso(this, s2)
            %from wikipedia:
            kl = 0.5 * ((trace(this.Sigma) + this.mu'*this.mu)/s2 - this.dim - this.ldSigma + this.dim * log(s2));
        end
    end

end
