% demo of preference learning
clear all
addpath(genpath('~/dev/gpml-matlab-v3.4-2013-11-11/'))

M = 4;
N = 1;
D = 2;
s2u = 1;
tau = 1;
hyp.s2u = s2u;

lik = likLogit();

rng(5);
% DRAW WEIGHTS
u = randn(D, N) * sqrt(hyp.s2u);

% DRAW DATA
V = rand(M, D);
z = V*u;
allPairs = nchoosek(1:M, 2);%all pairs without replacement
eta = z(allPairs(:,1),:) - z(allPairs(:,2),:);%utility differences
pY = likLogit.sigmoid(eta(:));%P(y=1|eta)
y = reshape(binornd(1,pY),size(allPairs,1), N );%binomial samples according to pY

% Convert into sparse matrix format
I = [];J = [];
for n = 1:N
    idxOne = y(:,n) == 1;
    idxZer = ~idxOne;
    pidx = [allPairs(idxOne,1); allPairs(idxZer, 2)];
    nidx = [allPairs(idxOne,2); allPairs(idxZer, 1)];
    pairIdx = sub2ind([M,M],pidx, nidx); 
    I = [I; pairIdx]; 
    J = [J; n*ones(size(pidx))];
end

y2 = y;
y2(y==0) = -1;
Y = sparse(I, J, ones(size(I)), M*M, N);

W = V(allPairs(:,1),:) - V(allPairs(:,2),:);

ys = Y(Y~=0);
Ws = V(pidx,:) - V(nidx,:);

mfopts = [];
%mfopts.numDiff = 2;
%mfopts.DerivativeCheck= 'on';
lambda0 = rand(size(W,1),1);
lambda = minFunc(@funDVI, lambda0, mfopts, W, y, lik,hyp);
lambdas = minFunc(@funDVI, lambda0, mfopts, Ws, ys, lik,hyp);
x0 = [lambda0; zeros(D,1)];
mfoptsOA.numDiff = 2;
x= minFunc(@funOpperArch, x0, mfoptsOA, Ws, ys, lik,hyp);
[fxPrim1, fxDual1] = dual2primal(W,lambda, y, lik, hyp)
[fxPrim2, fxDual2] = dual2primal(Ws,lambdas, ys, lik, hyp)
%[fxPrim5, fxDual5] = dual2primal(Ws,lambdas, ys, lik, hyp)


alpha = lambda - y;
A = W' * diag(lambda) * W + eye(D) / hyp.s2u;

L = chol(A,'lower');
Sigma = L' \ (L\eye(D));
mu = - hyp.s2u * W' * alpha;

alpha = lambdas - ys;
A = Ws' * diag(lambdas) * Ws + eye(D) / hyp.s2u;

L = chol(A,'lower');
Sigmas = L' \ (L\eye(D));
mus = - hyp.s2u * Ws' * alpha;


[f, g] = funDVI(lambda, W, y, lik, hyp);


hypGP.lik= [];
hypGP.s2u = s2u;
likGP = {@likLogistic};

postEPs = regEP(hypGP,  likGP, Ws, ys);
postEP = regEP(hypGP,  likGP, W, y2);

[fxPrim3, fxDual3] = dual2primal(Ws,postEPs.sW.^2, ys, lik, hyp)
fxPrim4  = lgmELBO(Ws,postEPs.Sigma, postEPs.mu, ys, lik, hyp)

p1 = likLogit.sigmoid(W * u);
p2 = likLogit.sigmoid(W * mu);
