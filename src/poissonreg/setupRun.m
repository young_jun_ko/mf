function [statsIter, opts]= setupRun(runName, statsIter, opts)
    statsIter.(runName).fIt = [];
    statsIter.(runName).xIt = {};
    statsIter.(runName).timeIt = [];
    opts.runName = runName;
