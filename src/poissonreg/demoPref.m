% demo of preference learning
clear all
addpath(genpath('~/dev/gpml-matlab-v3.4-2013-11-11/'))
addpath(genpath('~/dev/minFunc_2012'))
addpath(genpath('~/repos/mf/src/prettyPlot'))

M = 4;
N = 5;
D = 2;
s2u = 1;
tau = 1;
hyp.s2u = s2u;

lik = likLogit();

rng(1);
% DRAW DATA
V = [randn(M/2, D)*0.5 +  0.1;randn(M-M/2, D)*0.5 - 0.1];
V = randn(M,D) * 2

u = randn(D, N) * sqrt(hyp.s2u);

z = V*u;
allPairs = nchoosek(1:M, 2);%all pairs without replacement
eta = z(allPairs(:,1),:) - z(allPairs(:,2),:);%utility differences
pY = likLogit.sigmoid(eta(:));%P(y=1|eta)
y = reshape(binornd(1,pY),size(allPairs,1), N );%binomial samples according to pY

% Convert into sparse matrix format
I = [];J = [];

for n = 1:N
    idxOne = y(:,n) == 1;
    idxZer = ~idxOne;
    pidx = [allPairs(idxOne,1); allPairs(idxZer, 2)];
    nidx = [allPairs(idxOne,2); allPairs(idxZer, 1)];
    pairIdx = sub2ind([M,M],pidx, nidx); 
    I = [I; pairIdx]; 
    J = [J; n*ones(size(pidx))];
end

y2 = y;
y2(y==0) = -1;
Y = sparse(I, J, ones(size(I)), M*M, N);

W = V(allPairs(:,1),:) - V(allPairs(:,2),:);

ys = Y(Y~=0);
Ws = V(pidx,:) - V(nidx,:);

v0 = 0.1 * randn(size(V(:)));

mfopts = [];
%mfopts.numDiff = 2;
%mfopts.DerivativeCheck= 'on';
global statsIter

opts.maxItEM = 100;

mfopts.outputFcn = @recorder;
[statsIter, opts] = setupRun('dual', statsIter, opts);
tic;
[v,f,exitflag, out] = minFunc(@funPrefDual, v0, mfopts, opts,  Y, lik, hyp);
time1=toc;
V1 = reshape(v, M, D);

opts.maxItEM = out.funcCount;
[statsIter, opts] = setupRun('em', statsIter, opts);
[V2, elbo2]= prefEM(reshape(v0,M,D), Y, lik, hyp, opts);
time2 = toc;

elbo = 0 ;
Bn = Bpairs();

mfoptsE = [];
%mfoptsE.numDiff = 2;
%mfoptsE.DerivativeCheck= 'on';
mfoptsE.Display = 'off';

lik = likLogitExact();
for n = 1:N
    yn = Y(:,n);

    reset(Bn, yn);

    yObs = yn(yn~=0);
    W = mult(Bn, V1);
    lambda0 = rand(size(W,1),1);
    lambda = minFunc(@funDVI, lambda0, mfoptsE, W, yObs, lik,hyp);
    alpha = getAlpha(lik, lambda, yObs);
    [Sigma, mu, lda] = dual2posterior(W, lambda, alpha, hyp);
    [fx,h,rho] = lgmELBO(W, Sigma, mu, yObs, lik, hyp, lda);
    elbo = elbo + fx;
end
elbo1 = elbo;
elbo= 0;
for n = 1:N
    yn = Y(:,n);

    reset(Bn, yn);

    yObs = yn(yn~=0);
    W = mult(Bn, V2);
    lambda0 = rand(size(W,1),1);
    lambda = minFunc(@funDVI, lambda0, mfoptsE, W, yObs, lik,hyp);
    alpha = getAlpha(lik, lambda, yObs);
    [Sigma, mu, lda] = dual2posterior(W, lambda, alpha, hyp);
    [fx,h,rho] = lgmELBO(W, Sigma, mu, yObs, lik, hyp, lda);
    elbo = elbo + fx;
end
elbo2 = elbo;
elbo= 0;
for n = 1:N
    yn = Y(:,n);

    reset(Bn, yn);

    yObs = yn(yn~=0);
    W = mult(Bn, V);
    lambda0 = rand(size(W,1),1);
    lambda = minFunc(@funDVI, lambda0, mfoptsE, W, yObs, lik,hyp);
    alpha = getAlpha(lik, lambda, yObs);
    [Sigma, mu, lda] = dual2posterior(W, lambda, alpha, hyp);
    [fx,h,rho] = lgmELBO(W, Sigma, mu, yObs, lik, hyp, lda);
    elbo = elbo + fx;
end
elbo0 = elbo;

