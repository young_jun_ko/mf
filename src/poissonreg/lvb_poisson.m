function [f, gm, gv] = lvb_poisson(y, m, v, alpha)
% local variational bound to E[log p(y|eta)] 
% where expectation is wrt N(eta|m,v)
% For poisson, this is available exactly, so bound parameter 'alpha' is not used
% and there is no approximation.
%
% Written by Emtiyaz, EPFL
% Modified on Dec. 8, 2012

	m = m(:);
	v = v(:);
	y = y(:);
	EofA = exp(m + v/2);
	f = y.*m - EofA;
	gm = y - EofA; 
	gv = -0.5*EofA; 

