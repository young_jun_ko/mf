function [V, elbo] = prefEM(V0, Y, lik, hyp,opts)

    global statsIter
    [M2, N] = size(Y);
    [M,D] = size(V0);

    assert(M*M == M2)

    V = V0;
    Stats = cell(N,2);

    Bn = Bpairs();

    mfoptsE = [];
    %mfoptsE.numDiff = 2;
    %mfoptsE.DerivativeCheck= 'on';
    mfoptsE.Display = 'off';
    for it = 1:opts.maxItEM
        tic;
        elbo = 0;
        cnt = 0;
        %E-Step
        for n = 1:N
            yn = Y(:,n);

            reset(Bn, yn);

            yObs = yn(yn~=0);
            W = mult(Bn, V);
            lambda0 = rand(size(W,1),1);
            hypGP.lik= [];
            hypGP.s2u = hyp.s2u;
            likGP = {@likLogistic};
            postEP = regEP(hypGP,  likGP, W, yObs);
            alphaEP = -postEP.nu;
            lambdaEP = yObs - postEP.nu;
            lambda0 = lambdaEP;


            [lambda, f, exitflag, out ]= minFunc(@funDVI, lambda0, mfoptsE, W, yObs, lik,hyp);
            cnt = cnt + out.funcCount;
            alpha = getAlpha(lik, lambda, yObs);
            [Sigma, mu, lda] = dual2posterior(W, lambda, alpha, hyp);
            fx = lgmELBO(W, Sigma, mu, yObs, lik, hyp, lda);
%Sigma = postEP.Sigma;
%mu = postEP.mu;
            %fx = lgmELBO(W, Sigma, mu, yObs, lik, hyp);

            %Instead of Variational Gaussian we could also run EP:
            %hypGP.lik= [];
            %hypGP.s2u = hyp.s2u;
            %likGP = {@likLogistic};
            %postEP = regEP(hypGP,  likGP, W, yObs);

            elbo = elbo + fx;
            Stats{n, 1} = mu;
            Stats{n, 2} = Sigma;
        end
        fprintf('%03d %.5f avg Dual evals: %f\n' ,it, elbo, cnt/N)
        %M-Step
        mfoptsM = [];
        %mfoptsM.numDiff = 2;
        mfoptsM.Display = 'off';
        %mfoptsM.DerivativeCheck= 'on';
        v0 = V(:);
        v = minFunc(@funMStep, v0, mfoptsM, Y,Bn, Stats);
        V = reshape(v, M, D);
        timeIt = toc;

        statsIter.(opts.runName).fIt(end+1) = elbo;
        statsIter.(opts.runName).xIt{end+1} = V;
        statsIter.(opts.runName).timeIt(end+1) = timeIt;
    end

    function [fx, grad] = funMStep(v, y, Bn, Stats)
        [M2,N] = size(y);
        M = sqrt(M2);
        D = length(v) / M;
        V = reshape(v, M, D);

        fx = 0;

        grad = zeros(M,D);

        for n = 1:N
            yn = y(:,n);
            reset(Bn, yn);
            W = mult(Bn, V);
            yObs = yn(yn~=0);

            mu = Stats{n,1};
            Sigma = Stats{n,2};
            hn = W * mu;
            WSigma = W * Sigma;
            rhon = sum(W .* (WSigma), 2);

            expTerm = exp(hn + rhon * 0.5);
            fn = - yObs' * hn + sum(log1p(expTerm));

            fx = fx + fn;
            sigmoidn = expTerm ./ (1+expTerm);
            temp = multT(Bn, yObs - sigmoidn);

            gn = - temp * mu';
            temp = multT(Bn, diag(sigmoidn));
            grad = grad + gn + temp * WSigma;

        end
        grad = grad(:);
