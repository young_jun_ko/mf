function logPy = logPoisspdf(y, eta)
    % log of poisson pdf up to normalization parameterized in terms of eta=log(lambda)
    % log P(y | eta) = y * eta - exp(eta)


logPy = y .* eta - exp(eta);


