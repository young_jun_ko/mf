% demo of bayesian poisson multiple output regression
clear all;

addpath(genpath('~/dev/minFunc_2012'));

rng(1);

M = 10;
N = 100;
D = 2;
s2u = 1;
hyp.s2u = s2u;
opts.maxIter = 10;%iterations for variational poisson regression
mfopts = [];
%mfopts.DerivativeCheck = 'on';
mfopts.numDiff = 0;

% DRAW WEIGHTS
Vtrue = randn(M, D) * 0.15;
u = randn(D, N) * sqrt(hyp.s2u);

% DRAW DATA
btrue = 0;%randn(M, 1);
lambda = exp(Vtrue * u + btrue);
y = poissrnd(lambda);

% DRAW INITIAL VALUE
V0 = randn(M, D) * 1;
b = 0;%randn(M, 1) * 0.1;

% RUN OPTIMIZATION
v0 = V0(:);
v1 =v0;v2=v0;
global statsIter
opts.maxItEM = 10;
mfopts.outputFcn = @recorder;
[statsIter, opts] = setupRun('primal', statsIter, opts);
tic
v1 = minFunc(@funPoissonMulti, v0, mfopts, opts, y,hyp);

lik = likPoisson();
[statsIter, opts] = setupRun('em', statsIter, opts);
[Vem, elboEM] = prefEMDual(V0, y, lik, hyp,opts);
return;
[statsIter, opts] = setupRun('dual', statsIter, opts);
tic
v2 = minFunc(@funPoissonMultiDual, v0, mfopts,opts, y,hyp );

statsIter.primal.timeIt(end)
statsIter.dual.timeIt(end)

v1x = v1(1);
v1y = v1(2);

[X,Y] = meshgrid((v1x-0.01):0.001:(v1+0.01), (v1y-0.01):0.001:(v1y+0.01));
%vall = [X(:)'; Y(:)'];
%z1 = zeros(size(vall,2),1);
%z2 = zeros(size(vall,2),1);
%parfor it = 1:size(vall,2)
%    fprintf('%d/%d\n',it, size(vall,2))
%    vtemp = vall(:,it);
%    z1(it) = funPoissonMulti(vtemp, opts, y,hyp);
%    %z2(it) = funPoissonMultiDual(vtemp, opts, y,hyp);
%end
%Z1 = reshape(z1, size(X,1), size(X,2));
%Z2 = reshape(z2, size(X,1), size(X,2));
load Z.mat;

figure
contourf(X,Y,Z1)

hold on

for it =1:length(statsIter.primal.xIt)
    vit = statsIter.primal.xIt{it};
    plot(vit(1),vit(2), 'rx', 'MarkerSize', 10, 'LineWidth', 2)
end

plot(v1(1),v1(2), 'yx', 'MarkerSize', 12, 'LineWidth', 3)
figure
contourf(X,Y,Z1)

hold on

for it =1:length(statsIter.primal.xIt)
    vit = statsIter.em.xIt{it};
    plot(vit(1),vit(2), 'rx', 'MarkerSize', 10, 'LineWidth', 2)
end

plot(v2(1),v2(2), 'yx', 'MarkerSize', 12, 'LineWidth', 3)

%v12 = minFunc(@funPoissonMulti, v2, mfopts, y,hyp, opts);
%v21 = minFunc(@funPoissonMultiDual, v1, mfopts, y,hyp, opts);

V = V0;

% Stochastic gradient descent
%quite sensitive to step size 
eta = 0.01;
for it =1:8
    idx = randperm(N);
    for s = 1:N
        n = idx(s);
        yn = y(:,n);
        [fn, gn] = funPoissonSingleDual(V, yn, hyp, opts);
        V = V - eta * gn;
        %funPoissonMulti(V(:), y, hyp, opts) 
    end
    eta = eta * 0.95;
end
v3 = V(:);

% EVALUATE PRIMAL AND DUAL FOR SOLUTIONS
funPoissonMulti(Vtrue(:),opts,y, hyp)
funPoissonMultiDual(Vtrue(:),opts,y, hyp)
funPoissonMulti(v1,opts,y, hyp)
funPoissonMultiDual(v1,opts,y, hyp)
funPoissonMulti(v2,opts,y, hyp)
funPoissonMultiDual(v2,opts,y, hyp)
funPoissonMulti(v3,opts,y, hyp)
funPoissonMultiDual(v3,opts,y, hyp)

% COMPARE SPECTRA
V1 = reshape(v1,M,D);
V2 = reshape(v2,M,D);
V3 = reshape(v3,M,D);
[u0,l0] = eig(Vtrue' * Vtrue);
[u1,l1] = eig(V1' * V1);
[u2,l2] = eig(V2' * V2);
[u3,l3] = eig(V3' * V3);

[a0,b0,t0] = svd(Vtrue,0 );
[a1,b1,t1] = svd(V1,0 );
norm(Vtrue * Vtrue' - V1 * V1','fro') / norm(Vtrue,'fro')
[a2,b2,t2] = svd(V2,0 );
[a3,b3,t3] = svd(V3,0 );
