function [V, elbo] = prefEMDual(V0, Y, lik, hyp,opts)

    global statsIter
    [M2, N] = size(Y);
    [M,D] = size(V0);


    V = V0;
    Stats = cell(N,1);

    Bn = eye(M);

    mfoptsE = [];
    %mfoptsE.numDiff = 2;
    %mfoptsE.DerivativeCheck= 'on';
    mfoptsE.Display = 'off';
    for it = 1:opts.maxItEM
        tic;
        elbo = 0;
        cnt = 0;
        %E-Step
        for n = 1:N
            yn = Y(:,n);


            yObs = yn;
            W = V;
            lambda0 = rand(size(W,1),1);

[mu, Sigma, lambda, lda] = mfvb(yn, V,0 , hyp, opts);
            %[lambda, f, exitflag, out ]= minFunc(@funDVI, lambda0, mfoptsE, W, yObs, lik,hyp);
            %cnt = cnt + out.funcCount;
            %alpha = getAlpha(lik, lambda, yObs);
            %[Sigma, mu, lda] = dual2posterior(W, lambda, alpha, hyp);
            fx = lgmELBO(W, Sigma, mu, yObs, lik, hyp, lda);

            elbo = elbo + fx;
            Stats{n} = lambda;
        end
        fprintf('%03d %.5f avg Dual evals: %f\n' ,it, elbo, cnt/N)
        %M-Step
        mfoptsM = [];
        mfoptsM.numDiff = 2;
        %mfoptsM.Display = 'off';
        %mfoptsM.DerivativeCheck= 'on';
        v0 = V(:);
        v = minFunc(@funMStep, v0, mfoptsM, Y,Bn, Stats,hyp);
        V = reshape(v, M, D);
        timeIt = toc;

        statsIter.(opts.runName).fIt(end+1) = elbo;
        statsIter.(opts.runName).xIt{end+1} = V;
        statsIter.(opts.runName).timeIt(end+1) = timeIt;
    end

function [fx, grad] = funMStep(v, y, Bn, Stats,hyp)
    [M,N] = size(y);
    D = length(v) / M;
    V = reshape(v, M, D);

    fx = 0;

    grad = zeros(M,D);
    lik = likPoisson();

    for n = 1:N
        lambda = Stats{n};
        yn = y(:,n);
        W = V;
        yObs = yn;

        alpha = lambda - yObs;
        Wta = W' * alpha;



        [fxPrim, fxDual, Sigma, mu] = dual2primal(W,lambda, yObs, lik, hyp);

        grad = grad - hyp.s2u * alpha * Wta' + diag(lambda) * W * Sigma;
        fx = fx + fxDual;
    end
    grad = grad(:);
    fx = - fx;
