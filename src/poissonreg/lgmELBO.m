function [fx, h, rho] = lgmELBO(V, Sigma, mu, y, lik, hyp, varargin)
    % Evidence Lower Bound for matrix factorization
    % L(Q, V) = -KL[ Q(u) || P(u) ] - \sum_m f_m(h_m, rho_m)
    % where the current posterior Q(u) = N(mu, Sigma)
    %
    % INPUT:
    % - V: loading matrix
    % - y: data Mx1
    % - hyp.s2u: prior variance
    %
    % OUPUT
    % - fx: function value
    if length(varargin) == 1
        lda = varargin{1};
    else
        L = chol(Sigma,'lower');
        lda = -2 * sum(log(diag(L)));
    end

    %Primal objective
    h = V * mu;
    rho = sum(V .* (V * Sigma),2);
    f_lik = getF(lik, y, h, rho);
    fx = - 0.5*(lda+trace(Sigma)/hyp.s2u+(mu'*mu)/hyp.s2u)-f_lik;
