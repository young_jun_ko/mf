classdef Bpairs < handle
    properties
        B

    end
    methods(Static)
        function B = createB(M,pidx,nidx)
            Pn = length(pidx);
            if length(nidx)~= Pn
                error('sizes inconsistent: same number of positive and negative indices expected')
            end
            I = [1:Pn, 1:Pn]'; 
            J = [pidx; nidx];
            V = [ones(Pn,1); -ones(Pn,1)];
            B = sparse(I, J, V, Pn, M);
        end
        function [pidx, nidx,M] = getPairs(y)
            
            M2 = (length(y));
            M = sqrt(M2);
            assert(M*M == M2)
            obsIdx = find(y~=0);
            [pidx,nidx] = ind2sub([M,M], obsIdx);
        end
    end
    methods
        function obj = Bpairs()
        end
        function reset(this, y)
            [pidx, nidx,M] = Bpairs.getPairs(y);
            this.B = Bpairs.createB(M, pidx, nidx);
        end
        function y = mult(this, x)
            y = this.B * x;
        end
        function y = multT(this, x)
            y = this.B' * x;
        end
        function d = diagBtDB(this, lambda)
            d = sum(this.B' .* bsxfun(@times, this.B, lambda)',2);
        end
    end
end
