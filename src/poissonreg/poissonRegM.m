function [fx, grad] = poissonRegM(v,y,S,s)
    [M,N] = size(y);
    D = length(v)/M;
    V = reshape(v,M,D);
    grad = zeros(size(V));
    fx = 0;
    for m = 1:M
        vm = V(m,:)';
        gradm = zeros(size(vm));
        for n = 1:N
            ymn = y(m,n);
            Sn = S(:,:,n);
            sn = s(:,n);
            Sv = Sn * vm;
            qd = vm' * Sv;
            lin = vm' * sn;
            fx = fx - ymn * lin + exp(0.5 * qd + lin);
            gradm = gradm - ymn * sn + (sn + Sv) * exp(0.5 * qd + lin);
        end
        grad(m,:) = gradm';

    end
    grad = grad(:);
