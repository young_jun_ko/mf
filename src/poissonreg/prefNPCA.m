function [V, elbo] = prefNPCA(V0, Y, lik, hyp,opts)
    
    [M2, N] = size(Y);
    [M,D] = size(V0);

    assert(M*M == M2)

    V = V0;
    Stats = cell(N,2);

    Bn = Bpairs();

    mfoptsE = [];
    %mfoptsE.numDiff = 2;
    %mfoptsE.DerivativeCheck= 'on';
    mfoptsE.Display = 'off';

    %memory for sufficient statistics
    S = zeros(M);
    s = zeros(M,1);

    for it = 1:opts.maxItEM
        elbo = 0;
        %E-Step
        for n = 1:N
            yn = Y(:,n);

            reset(Bn, yn);

            yObs = yn(yn~=0);
            W = mult(Bn, V);
            lambda0 = rand(size(W,1),1);
            lambda = minFunc(@funDVI, lambda0, mfoptsE, W, yObs, lik,hyp);
            alpha = getAlpha(lik, lambda, yObs);
            [Sigma, mu, lda] = dual2posterior(W, lambda, alpha, hyp);
            [fx,h,rho] = lgmELBO(W, Sigma, mu, yObs, lik, hyp, lda);

            A = 1./diag(lambda) + W * W';
            L = chol(A,'lower');
            Linv = L \ eye(size(L));
            e = L' \ (L \ diag(-alpha ./ lambda)));
            BtLinv = multT(Bn, Linv');
            BtEB = BtLinv * BtLinv';
            Bte = multT(Bn, e);
            S = S + BtEB - Bte * Bte';
            s = s + Bte;

            %Instead of Variational Gaussian we could also run EP:
            %hypGP.lik= [];
            %hypGP.s2u = hyp.s2u;
            %likGP = {@likLogistic};
            %postEP = regEP(hypGP,  likGP, W, yObs);

            elbo = elbo + fx;
        end

        fprintf('%03d %.4f\n' ,it, elbo)

        %M-Step
        Sigma = V * V';
        C = Sigma - Sigma * (S/N + s * s') * Sigma;

        [U, T] = eig(C);
        t = diag(T);
        [~, sidx] = sort(t, 'descend');
        U = U(:,sidx);

    end

