% demo of bayesian logit regression
clear all
addpath(genpath('~/dev/gpml-matlab-v3.4-2013-11-11/'))

M = 1000;
N = 1;% later we may want to do multiple output regression
D = 2;
s2u = 1;
hyp.s2u = s2u;

lik = likLogit();

rng(1);
% DRAW WEIGHTS
u = randn(D, N) * sqrt(hyp.s2u);

% DRAW DATA
V = rand(M, D);
%V = eye(D);
b = 0;% randn(M, 1)*3;
eta = (V*u + b);
p1= likLogit.sigmoid( eta);
y = binornd(1, p1);
y2 = y;
y2(y ==0 ) = -1;

opts.maxIter = 120;
%[mu, Sigma, lambda1] = mfvb(y, V,b, hyp, opts);
mfopts = [];
%mfopts.numDiff = 2;
lambda0 = rand(size(V,1),1);
%lambda = minFunc(@funPoissonRegDVI, lambda0, mfopts,V, y, lik,hyp);
lambda = minFunc(@funDVI, lambda0, mfopts,V, y, lik,hyp);

alpha = lambda - y;
A = V' * diag(lambda) * V + eye(D) / hyp.s2u;

L = chol(A,'lower');
SigmaD = L' \ (L\eye(D));
muD = - hyp.s2u * V' * alpha;

[f, g] = funDVI(lambda, V, y, lik, hyp);

%[f1,g1]=funObj_decoupled(lambda, y, @lvb_poisson, zeros(D,1),s2u * eye(D) , N, [], []);

hypGP.cov = [];
hypGP.mean = [0];
hypGP.lik= [];
hypGP.s2u = s2u;
meanFun = {@meanConst};
covFun = {@covLIN};

likGP = {@likLogistic};
[post,nlZ,dnlZ] = infParEP(hypGP, meanFun, covFun, likGP, V, y2);
%[post2,nlZ,dnlZ] = infEP(hypGP, meanFun, covFun, likGP, V, y2);
postEP = regEP(hypGP,  likGP, V, y2);

Aep = eye(D) + V' * diag(post.ttau) * V;
muep = A \ (V' * post.tnu);


p1 = likLogit.sigmoid(V * u);
p2 = likLogit.sigmoid(V * muD);
