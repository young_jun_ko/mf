classdef likPoisson < handle
    methods
        function obj = likPoisson()
        end

        % calculates the convex conjugate and its derivative
        % f*(lambda) = \max_{h, rho} alpha'*h + lambda'*rho/2 - \sum_n f_n(h_n, rho_n)
        % where f is defined by this.getF()
        function [con, dcon] = cvxCon(this, lambda)
            dcon = log(lambda);
            con = lambda' * (dcon - 1);
        end

        % calculates f = E_eta [ -log P(y | eta) ]
        % with Q(eta) = N(h,R) and rho = diag(R)
        function [f] = getF(this, y, h, rho)
            f = -y'*h + sum(exp(h+rho*0.5));
        end
        function [alpha] = getAlpha(this, lambda, y)
           alpha = lambda - y; 
        end
    end
end
