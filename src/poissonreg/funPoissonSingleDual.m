function [fx, grad] = funPoissonSingleDual(V, y, hyp, opts)
    % Single user objective function for Poisson matrix factorization
    % L(V) = 0.5*a'*VV'a - 0.5*logdet(A) + sum f*(a, lam)
    %
    % INPUT:
    % - V: latent factors MxD
    % - y: data vector Mx1
    % - hyp.s2u: prior variance
    % - opts.maxIter: parameters for inference algorithm
    %
    % OUPUT
    % - fx: function value
    % - grad: gradient wrt. V, MxD

    D = size(V,2);

    lik = likPoisson();
    % variational inference, so far poisson specific
    if ~isfield(opts, 'dvi') || opts.dvi == 0
        [~, Sn, lambda, lda] = mfvb(y, V, 0, hyp, opts);
        alpha = getAlpha(lik, lambda, y);
    else
        mfopts.Display = 'off';
        %mfopts.numDiff = 2;

        lambda0 = rand(size(V,1),1);
        lambda = minFunc(@funDVI, lambda0, mfopts,V, y, lik,hyp);
        alpha = getAlpha(lik, lambda, y);
        [Sn, sn, lda] = dual2posterior(V, lambda,alpha, hyp);
    end
    Vta = V' * alpha;
    grad = -hyp.s2u *alpha * Vta' + diag(lambda) * V * Sn; 
    f_star = sum(lambda.*(log(lambda)-1));
    fxDual = 0.5*(hyp.s2u*(Vta'*Vta)-lda-D)+f_star;

    fx = -fxDual;%negate because we want to minimize it

