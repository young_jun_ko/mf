% demo of bayesian gaussian process poisson multiple output regression
clear all;

addpath(genpath('~/dev/minFunc_2012'));
addpath(genpath('~/dev/gpml-matlab-v3.4-2013-11-11'));
rng(1);

M = 3;
N = 15;
D = 2;
s2u = 1;
hyp.s2u = s2u;
opts.maxIter = 10;%iterations for variational poisson regression
hypK = [0 0];
mfopts = [];
%mfopts.DerivativeCheck = 'on';
mfopts.numDiff = 0;

% DRAW WEIGHTS
u = randn(D, N) * sqrt(hyp.s2u);

% DRAW DATA
Vtrue = randn(M, D);
%K = covSEiso(hypK, Vtrue);
K = covLIN([], Vtrue) + eye(M) ;
LK = chol(K, 'lower');

f = LK * randn(M, N);

lambda = exp(f);
y = poissrnd(lambda);

% DRAW INITIAL VALUE
V0 = randn(M, D) * 0.1;

% RUN OPTIMIZATION
v0 = V0(:);
v0 = Vtrue(:);
v1 = minFunc(@funPoissonMulti, v0, mfopts, y,hyp, opts);
v2 = minFunc(@funPoissonMultiDual, v0, mfopts, y,hyp, opts);

V = V0;

%quite sensitive to step size 
eta = 0.05;
for it = 1:500
    idx = randperm(N);
    for s = 1:N
        n = idx(s);
        yn = y(:,n);
        [fn, gn] = funPoissonSingleDual(V, yn, hyp, opts);
        V = V - eta/(it) * gn;
   %     funPoissonMulti(V(:), y, hyp, opts) 
    end
end
v3 = V(:);

% EVALUATE PRIMAL AND DUAL FOR SOLUTIONS
%funPoissonMulti(Vtrue(:), y, hyp, opts)
%funPoissonMultiDual(Vtrue(:), y, hyp, opts)
funPoissonMulti(v1, y, hyp, opts)
funPoissonMultiDual(v1, y, hyp, opts)
funPoissonMulti(v2, y, hyp, opts)
funPoissonMultiDual(v2, y, hyp, opts)
funPoissonMulti(v3, y, hyp, opts)
funPoissonMultiDual(v3, y, hyp, opts)

% COMPARE SPECTRA
V1 = reshape(v1,M,D);
V2 = reshape(v2,M,D);
V3 = reshape(v3,M,D);
[u0,l0] = eig(Vtrue' * Vtrue);
[u1,l1] = eig(V1' * V1);
[u2,l2] = eig(V2' * V2);
[u3,l3] = eig(V3' * V3);



