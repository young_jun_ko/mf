function [fx, grad, funcCount, ldiff, adiff] = funPrefSingleDual(Bn, V, y, lik, hyp)
    % Single user objective function for Poisson matrix factorization
    % L(V) = 0.5*a'*VV'a - 0.5*logdet(A) + sum f*(a, lam)
    %
    % INPUT:
    % - V: latent factors MxD
    % - y: data vector Mx1
    % - hyp.s2u: prior variance
    % - opts.maxIter: parameters for inference algorithm
    %
    % OUPUT
    % - fx: function value
    % - grad: gradient wrt. V, MxD

    D = size(V,2);

    W = mult(Bn, V);
    yObs = y(y~=0);

    mfopts = [];
    %mfopts.numDiff = 2;
    mfopts.maxFunEvals= 1e6;
    %mfopts.DerivativeCheck= 'on';
    mfopts.Display ='off';
    lambda0 = rand(size(W,1),1);
    hypGP.lik= [];
    hypGP.s2u = hyp.s2u;
    likGP = {@likLogistic};
    %postEP = regEP(hypGP,  likGP, W, yObs);
    postEP = infKL(hypGP,  likGP, W, yObs);

    %rho = sum(W' .* (postEP.Sigma* W'))';
    %h =  W * postEP.mu;

    rho = diag(postEP.Sigma);
    h =  postEP.mu;
    sigmoEP = likLogit.sigmoid(h + rho * 0.5);
    alphaEP = sigmoEP - yObs;
    lambdaEP = sigmoEP;
    lambda0 = sigmoEP;

    [lambda, f, exitflag, out ]= minFunc(@funDVI, lambda0, mfopts, W, yObs, lik,hyp);
    lambda
    postEP.tau
    postEP.nu
out.iterations
    [fxPrim, fxDual, Sigma, mu] = dual2primal(W,lambda, yObs, lik, hyp);

    [lambdaFP, alphaFP, SigmaFP, muFP] = testFP(lambda0, W, yObs, hyp);

    [fxPrimFP, fxDualFP ] = dual2primal(W,lambdaFP, yObs, lik, hyp)
    pause
    %[lambdaFP2, alphaFP2, SigmaFP, muFP] = testFP(lambda, W, yObs, hyp);
    %rho = diag(W * SigmaFP * W');
    %h = W * muFP;
    %sigmoFP = likLogit.sigmoid(h + rho * 0.5);
    %rho = diag(W * Sigma * W');
    %h = W * mu;
    %sigmo = likLogit.sigmoid(h + rho * 0.5);
    %[norm(lambda- sigmo), norm(lambdaFP-sigmoFP)]


    alpha = getAlpha(lik, lambda, yObs);
    funcCount = out.iterations;

    [fxPrimEP, fxDualEP, SigmaEP, muEP] = dual2primal(W,lambdaEP, yObs, lik, hyp);
    %fprintf('%f %f %f %f; %f %f\n', fxPrim, fxDual, fxPrimEP, fxDualEP, norm(alpha-alphaEP), norm(lambda-lambdaEP))
    %ldiff = norm(postEP.tau- lambda);
    ldiff = norm(postEP.mu - mu);
    adiff = norm(alphaEP - alpha);
    Vta = W' * alpha;
    halpha = multT(Bn, alpha);
    hLambda = multT(Bn, diag(lambda));
    grad = -hyp.s2u *halpha * Vta' + hLambda * W * Sigma; 


    %grad = multT(Bn, alphaFP) * muFP' + multT(Bn,  diag(lambdaFP)) * W * SigmaFP;

    %f_star = cvxCon(lik, lambda);
    %fxDual = 0.5*(hyp.s2u*(Vta'*Vta)-lda-D)+f_star;

    fx = -fxDual;%negate because we want to minimize it

