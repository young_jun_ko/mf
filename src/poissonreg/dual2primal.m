function [fxPrim, fxDual, Sigma, mu] = dual2primal(V,lambda, y, lik, hyp)
    % Calculate primal objective (ELBO) from dual variational parameters
    %
    % INPUT:
    % - V: loading matrix
    % - y: data Mx1
    % - hyp.s2u: prior variance
    % - opts.maxIter: parameters for inference algorithm
    %
    % OUPUT
    % - fxPrim: primal function value
    % - fxDual: dual function value
    % - Sigma: posterior covariance
    % - mu: posterior mean

    [M,D] = size(V);
    alpha = getAlpha(lik, lambda, y);

    %posterior moments:
    [Sigma, mu, lda] = dual2posterior(V, lambda, alpha, hyp);

    %Primal objective
    fxPrim = lgmELBO(V, Sigma, mu, y, lik, hyp, lda);

    %Dual objective
    Vta = V' * alpha;
    f_star = cvxCon(lik, lambda);
    fxDual = 0.5*(hyp.s2u*(Vta'*Vta)-lda-D)+f_star;
