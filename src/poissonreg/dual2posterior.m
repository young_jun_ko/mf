function [Sigma, mu, lda] = dual2posterior(V, lambda,alpha, hyp)
    [M,D] = size(V);
    A = V' * diag(lambda) * V + eye(D) / hyp.s2u;
    L = chol(A,'lower');
    Sigma = L' \ (L\eye(D));
    mu = - hyp.s2u * V' * alpha;
    lda = 2 * sum(log(diag(L)));
