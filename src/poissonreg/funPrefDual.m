function [fx, grad ] = funPrefDual(v, opts, y, lik, hyp)
    %Objective function for Poisson multiple output regression using the dual formulation
    % L(V) = 0.5 * a'*VV'a - 0.5*logdet(A)
    %
    % INPUT:
    % - v: vector representing V in column major order, i.e. v = V(:)
    % - y: data MxN
    % - hyp.s2u: prior variance
    % - opts.maxIter: parameters for inference algorithm
    %
    % OUPUT
    % - fx: function value
    % - grad: gradient wrt. V

    [M2,N] = size(y);
    M = sqrt(M2);
    D = length(v) / M;
    V = reshape(v, M, D);

    fx = 0;
    grad = zeros(M,D);

    Bn = Bpairs();
cnt = 0;
ad = 0;
ld = 0;
    for n = 1:N
        yn = y(:,n);
        reset(Bn, yn);
        [fn, gn, fcount, ldiff, adiff] = funPrefSingleDual(Bn, V, yn, lik, hyp);
        fx= fx + fn;
        grad = grad + gn;
        cnt = cnt + fcount;
        ad = ad + adiff;
        ld = ld + ldiff;
    end
    grad = grad(:);
    fprintf('avg Dual evals: %f, %f %f\n', cnt/N, ad/N, ld/N)
