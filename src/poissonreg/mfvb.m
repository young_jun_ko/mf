function [mu, Sigma, lambda, ldA] = mfvb(y, V, bias, hyp, opts)
    %mean field variational bayesian inference for poisson regression following
    % Luts, Wand - "Variational inference for count response semiparametric regression" 
    %
    %INPUT:
    % - y: count data
    % - V: design matrix
    % - hyp: hyper parameters
    %   - hyp.s2u: u ~ N(0, s2u*I)
    % - opts: algorithm parameters
    %   - opts.maxIter
    %
    % OUTPUT
    % - Gaussian posterior N(mu, Sigma)
    % - lambda: variational parameters
    % - ldA: log(det(inv(Sigma)))

    D = size(V,2);
    Sigma = eye(D) * hyp.s2u; %Sigma = Sigma * Sigma' + eye(D) * hyp.s2u;
    mu = zeros(D,1);

    for it = 1:opts.maxIter
        rho = sum(V .* (V * Sigma),2);
        h = V * mu;
        lambda = exp(h + 0.5*rho + bias);
        A = (V' * diag(lambda) * V + eye(length(mu))/hyp.s2u);
        b = (V' * (y - lambda) - mu / hyp.s2u);%b = -nabla_mu L
        try
            L = chol(A,'lower');
        catch e
            it
            rho'
            mu'
            lambda'
            V'
            V' * diag(lambda) * V
            eig(A)
            e
            error('cholesky')
        end
        muTemp = L' \ (L\ b);
        mu = mu + muTemp;
        Sigma = L' \ (L \ eye(D));
    end
    ldA = 2 * sum(log(diag(L)));
end
