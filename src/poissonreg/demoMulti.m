% demo of bayesian poisson multiple output regression
addpath(genpath('~/dev/minFunc_2012'));

rng(1);

M = 4;
N = 10;% later we may want to do multiple output regression
D = 2;
s2u = 1;
opts.maxIter = 10;
iterEM = 50;
iterFP = 10;
hyp.s2u = s2u;
mfopts.DerivativeCheck = 'off';
mfopts.numDiff = 2;

% DRAW WEIGHTS
u = randn(D, N) * sqrt(hyp.s2u);

% DRAW DATA
Mhalf = floor(M/2);
Vtrue = [randn(Mhalf, D) * 1.1; randn(M - Mhalf, D)*0.4];
btrue = 0;%randn(M, 1);
lambda = exp(Vtrue * u + btrue);
y = poissrnd(lambda);


V0 = randn(M, D) * 0.1;
b = 0;%randn(M, 1) * 0.1;
V = V0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%
%%%%%% DEPRECATED See demoPoissonMLE.m
%%%%%% DEPRECATED See demoPoissonMLE.m
%%%%%% DEPRECATED See demoPoissonMLE.m
%%%%%% DEPRECATED See demoPoissonMLE.m
%%%%%% DEPRECATED See demoPoissonMLE.m
%%%%%% DEPRECATED See demoPoissonMLE.m
%%%%%% DEPRECATED See demoPoissonMLE.m
%%%%%% DEPRECATED See demoPoissonMLE.m
%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

v0 = V0(:);
v1 = minFunc(@funPoissonMulti, v0, mfopts, y,hyp, opts);
V1 = reshape(v1, M,D);

return
v2 = minFunc(@funPoissonMultiDual, v0, mfopts, y,hyp, opts);
V2 = reshape(v2, M,D);

for it = 1:iterEM

    s = zeros(D, N);
    S = zeros(D,D, N);
    lambda = zeros(M,N);

    for n = 1:N
        [s(:,n), S(:,:,n), lambda(:,n)] = mfvb(y(:,n), V,0, hyp, opts);
    end

    v1 = randn(D*M,1);


    vout2 = minFunc(@poissonRegM, v1, mfopts, y,S,s);
    V = reshape(vout2, M,D);

end

