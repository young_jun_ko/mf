function r = gmm(x,pi1,mu1,sigma1,pi2,mu2,sigma2)

r1 = 1/sqrt(2*pi*sigma1^2)*exp(-(x-mu1).^2./(2*sigma1^2));
r2 = 1/sqrt(2*pi*sigma2^2)*exp(-(x-mu2).^2./(2*sigma2^2));
r = pi1*r1 + pi2*r2;