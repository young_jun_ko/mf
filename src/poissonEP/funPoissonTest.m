function pyw = funPoissonTest(w,y, X, s2w)

    eta = X * w;

    lambda = max(eta,0);


    Y = repmat(y, 1, length(w));
    pLk = PoissonUnnormalized(Y,lambda);
    %    pLk(eta<=0) = 0;
    pyw = prod(pLk,1) .* exp( -0.5 * (w.*w) / s2w) ;
    assert(~any(pyw<0))

end

function lp = PoissonUnnormalized(y, lambda)

    %lp = (lambda .^ y) .* exp(-lambda - gammaln(y+1)) ;
    lp = (lambda .^ y) .* exp(-lambda);

    %lp = y .* log(lambda) - lambda;
end
