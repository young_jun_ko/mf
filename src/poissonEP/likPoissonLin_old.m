function [lp, dlp, d2lp] = likPoissonLin(hyp, y, hCav, rhoCav, inf)
    rho = rhoCav;
    h = hCav - rhoCav;

   [lp,dlp,d2lp] = mexGetIy(y, h, rho);

    return;

    logh = log(abs(h));
    logrho = log(rho);
    kappa = -h / sqrt(rho);
    hk = hazard(kappa);

    logIy = zeros(y+1,1);

    logIy(1) = 0;
    logIy(2) = log(h + sqrt(rho)*hk);


    %Accumulate log partition
    if h == 0 
        for r = 2:1:y
            logIy(r+1) = log(r-1) + logrho + logIy(r-1);
        end
    else
        if h < 0
            %we have h < 0 so (r-1) * rho * Iy(r-1) - abs(h) * Iy(r)
            %we have to assume that the first term is larger 
            for r = 2:1:y
                loga = log(r-1) + logrho + logIy(r-1);
                logb = logh + logIy(r);
                logIy(r+1) = loga  + log1p(- exp(logb - loga));

            end
        else
            %we have h > 0
            %we need to check which term is larger
            for r = 2:1:y
                loga = log(r-1) + logrho + logIy(r-1);
                logb = logh + logIy(r);
                if loga > logb
                    logIy(r+1) = loga + log1p(exp(logb - loga));
                else
                    logIy(r+1) = logb + log1p(exp(loga - logb));
                end
                % fprintf('%f\n', logIy(r+1) )
            end
        end
    end

    Z0 = (0.5 * erfc(-h/sqrt(rho * 2)));
    Iy = zeros(y+1,1);
    Iy(1) = Z0;
    Iy(2) = Z0 * (h + sqrt(rho)*hk);
    for r = 2:1:y
        temp = (r-1) * rho * Iy(r-1) + h * (Iy(r));
        Iy(r+1) = (temp);
    end

    %logIy = log(Iy);
    f = @(x) Eqn(y, h, rho, x);
    a = integral(f,0,inf);
    fprintf('%.16e  \n',logIy(end) -log(a) )
    %logIy(end) = log(a);

    %Accumulate derivatives
    Ly = zeros(y+1,1);

    Ly(1) = hk/sqrt(rho);
    for r = 1:y
        Ly(r+1) = r / (h + rho * Ly(r));
    end
%    Ly(end) =y* exp(logIy(end-1)) / exp(logIy(end));

    %data term log(y!) = sum(log(1:y))
    logI0 = log(0.5 * erfc(kappa/sqrt(2)));


    lp = logIy(end) + logI0 - hCav + 0.5 * rhoCav ;
    dlp = Ly(end) - 1;
    d2lp =0;
    d2lp = -Ly(end) * ( Ly(end) - Ly(end-1)); 


function y = Eqn(n,h,rho,s)

    Z0 = (0.5 * erfc(-h/sqrt(rho * 2)));
    % evaluates normpdf(s, h, sqrt(rho)) * s^n for s>=0
    if s < 0
        y=0;
    else
        %y = s.^n .* normpdf((s-h)./sqrt(rho));
        y = n .* log(s) - 0.5 * (s-h).^2 / rho - 0.5 * log(2*pi*rho) - log(Z0);
        y =exp(y);
    end


