% Sets up the Gaussian mixture model test case of Section 5 of 
% http://arxiv.org/pdf/1401.8274v1.pdf For demonstration purposes these
% data are then unfolded using Tikhonov regularization. The most important
% output are:
%
% y       smeared data on a histogram with nBinsF bins on interval [lbF,ubF]
% K       smearing matrix corresponding to convolving with a Gaussian
% f       intensity function of the true spectrum evaluated on gridE
% g       intensity function of the smeared spectrum evaluated on gridF
% omega   precision matrix of the Gaussian smoothness prior
% omegaA  as above but with the Aristotelian boundary conditions
%
%
% Author: Mikael Kuusela (e-mail: mikael.kuusela@epfl.ch)

rng(1234);

clear;
close all;
load gmmSetup20k.mat
%{  
nBinsF = 40; % Number of bins for the smeared histogram
lbF = -7; % Lower bound of the smeared histogram
ubF = 7; % Upper bound of the smeared histogram
binsF = linspace(lbF,ubF,nBinsF+1); % Binning of the smeared histogram

lbE = -7; % Lower bound of the true space
ubE = 7; % Upper bound of the true space
order = 4; % Cubic splines
nKnots = 26; % Number of knots for the spline expansion
knots = linspace(lbE,ubE,nKnots+2);
knots = augknt(knots,order);
p = nKnots + order;

% Set up evaluation grids for the intensity functions
nGridE = 500;
gridE = linspace(lbE,ubE,nGridE);
nGridF = 500;
gridF = linspace(lbF,ubF,nGridF);

lambdaTot = 20000; % Expected total number of observations
%lambdaTot = 1000; % Expected total number of observations

% Parameters of the Gaussian mixture model
pi1 = 0.2;
mu1 = -2;
sigma1 = 1;
pi2 = 0.5;
mu2 = 2;
sigma2 = 1;
pi3 = 0.3;

sigmaConv = 1; % Standard deviation of the convolving Gaussian

% Generate the smeared data
N = poissrnd(lambdaTot);
Ncomp = mnrnd(N,[pi1 pi2 pi3]);
xPoint = [normrnd(mu1,sigma1,1,Ncomp(1)) normrnd(mu2,sigma2,1,Ncomp(2)) unifrnd(lbE,ubE,1,Ncomp(3))];
yPoint = xPoint + normrnd(0,sigmaConv,1,N);
y = histc(yPoint,binsF)';
y = y(1:end-1);

% Construct smearing matrix K
K = zeros(nBinsF,p);
for i=1:nBinsF
    ki = @(x)(normcdf(binsF(i+1),x,sigmaConv) - normcdf(binsF(i),x,sigmaConv));
    for j=1:p
        beta = zeros(1,p);
        beta(j) = 1;
        Bj = spmak(knots,beta);
        fun = @(x)(ki(x).*fnval(Bj,x));
        intLb = knots(j);
        intUb = knots(j+order); % NB: need to check that this works when order != 4
        K(i,j) = quadgk(fun,intLb,intUb);
    end
end

% Store true intensity f
fFun = @(s) gmm(s,pi1,mu1,sigma1,pi2,mu2,sigma2) + pi3*1/(ubE-lbE);
f = lambdaTot*fFun(gridE);

% Store smeared intensity g
g = zeros(1,nGridF);
for i=1:nGridF
    t = gridF(i);
    integrand = @(s) normpdf(t,s,sigmaConv).*fFun(s);
    g(i) = lambdaTot*quadgk(integrand,lbE,ubE);
end

% Construct prior precision omega
omega = zeros(p,p);
for i=1:p
    beta1 = zeros(1,p);
    beta1(i) = 1;
    Bi = spmak(knots,beta1);
    D2Bi = fnder(Bi,2);
    for j=1:p
        beta2 = zeros(1,p);
        beta2(j) = 1;
        Bj = spmak(knots,beta2);
        D2Bj = fnder(Bj,2);
        D2BiD2Bj = fncmb(D2Bi,'*',D2Bj);
        omega(i,j) = diff(fnval(fnint(D2BiD2Bj),[lbE ubE]));
    end
end
save gmmSetup20k.mat
return
%}

% Construct augmented omega (Aristotelian boundary conditions)
gammaLeft = 5;
gammaRight = 5;
omegaA = omega;
omegaA(1,1) = omegaA(1,1) + gammaLeft;
omegaA(end,end) = omegaA(end,end) + gammaRight;

saveImg = true;
% Tikhonov regularization (for demonstration only, actual implementation should also take into account the heteroscedasticity of the data)
L = chol(omegaA);
delta = 1e-4;
betaHat = [K; sqrt(delta)*L] \ [y; zeros(p,1)];

hyp.delta = delta;
hyp.omegaA = omegaA;
hyp.lik = [];
lik = { @likPoissonLin };
Deltas = 10.^(-3:-1:-12);
Deltas = 1e-7:1e-7:2e-6;
nlZs = [];
del = 8.5e-8;
del = 1e-6;
del = 5.0864e-7;
%for d = 1:length(Deltas)
for d = 1:1
    %hyp.delta = Deltas(d);
    hyp.delta = del;
    tic;
    [post,nlZ,dnlZ] = runPoissonEP(hyp, [], [], lik, K, y , betaHat);
    toc
    Ebb = post.muB * post.muB' + post.SigmaB;
    del = p/ (trace( Ebb * omegaA))

    %pause
    nlZs(end+1) = nlZ;
end
    % Construct the estimated intensity
    fHat = spmak(knots,betaHat');
    fHatGridE = fnval(fHat,gridE);

    fHat2 = spmak(knots,post.muB');
    fHatGridE2 = fnval(fHat2,gridE);


%}
%return
% Plot intensities
f1 = figure;
hold on;
plot(gridE,fHatGridE,'b');
plot(gridE,fHatGridE2+corr','r');
plot(gridE,f,'k');
plot(gridF,g,'m');
hold off;
ylim([0,1.1*max(f)]);
xlim([lbE ubE]);
box on;
leg = legend('Unfolded intensity','Unfolded intensity (EP)','True intensity','Smeared intensity','Location','Northwest');
set(leg,'FontSize',9);
ylabel('Intensity');

if saveImg
    filename = '~/repos/mf/src/poissonEP/unfolded.pdf';
    saveas(f1,filename)
    system(['pdfcrop --pdftexcmd pdflatex ', filename, ' ' , filename])
end


f2 = figure;
imagesc(post.SigmaB)
axis equal
colorbar
title('Posterior Covariance Cov[ \beta | y ]')

if saveImg
    filename = '~/repos/mf/src/poissonEP/postCov.pdf';
    saveas(f2,filename)
    system(['pdfcrop --pdftexcmd pdflatex ', filename, ' ' , filename])

end


