function y = hazard(x)
    %computes h(x) = normpdf(x)/1-normcdf(x)
    logy = - 0.5 * (x*x + log(2*pi)) - log(0.5 * erfc(x/sqrt(2)));
    y = exp(logy);
    %y2 = normpdf(x) / (1-normcdf(x));

