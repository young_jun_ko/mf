function [postW] = infGauss(X,y,postW,params)
    %Bayesian linear regression

    D = length(params.prior.mu);

    py = 1./params.s2l;

    A =  X' * diag(py) * X + params.prior.Omega;

    b = X' * (py .* y) + params.prior.Omega * params.prior.mu;

    L = chol(A,'lower');


    postW.m = L' \ (L \ b);
    postW.V = L' \ (L \ eye(D));

    ldV = -2 * sum(log(diag(L)));

end

