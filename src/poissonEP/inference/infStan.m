function [postW] = infStan(X,y,postW,params)
    %Bayesian linear regression

    D = length(params.prior.mu);


    standata.X = X;
    standata.y = y;
    standata.sdw = sqrt(s2w);
    standata.D = length(w);
    standata.M = length(y);

    if D == 1
        fit = stan('file', 'inference/poissonReg1D.stan', 'data', standata);
    else
        fit = stan('file', 'inference/poissonReg.stan', 'data', standata);
    end

end
