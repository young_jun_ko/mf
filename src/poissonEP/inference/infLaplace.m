function [postW] = infLaplace(X,y,postW,params)

    %MAP
    mfOpts = [];
    mfOpts.Display = 'on';
    %mfOpts.numDiff = 2;
    %mfOpts.DerivativeCheck = 'on';
    w0 = postW.m;

    wmap = minFunc(@negLogJoint, w0, mfOpts, X,y,params);

    %Hessian for covariance, function value for logZ (norm(g) is close to 0)
    [f,g,H] = negLogJoint(wmap, X,y,params);

    postW.m = wmap;
    postW.V = inv(H);

    L= chol(H, 'lower');
    ldH = 2 * sum(log(diag(L)));

    postW.logZ = -f - 0.5 * ldH;

end


function [f,g,H] = negLogJoint(w, X,y,params)

    eta = X * w;

    r = w - params.prior.mu;

    f = - (sum(y.*log(eta) - eta) - 0.5 * dot(r, params.prior.Omega*r)) + sum(gammaln(y+1)) - 0.5 * params.prior.ldOmega;

    if nargout > 1

        g = -((X' * (y./eta - 1)) - params.prior.Omega * r);

        if nargout > 2

            H = X' * diag(y ./eta) * X + params.prior.Omega;

        end
    end
end
