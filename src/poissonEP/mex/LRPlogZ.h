#ifndef LRPLOGZ_H
#define LRPLOGZ_H

#include <armadillo>

using arma::vec;

double getLogZy(const int y, const double h, const double rho, double &alpha, double &nu, vec &logIr);

/**
 * Computes the 0th, 1st and 2nd moments of the tilted distribution
 * 
 *     log P(s) = log P(y | s) + log Q_(s) - logZ
 * 
 * for a linear rate poisson likelihood
 * 
 *     log P(y | s) = y*log(s) - s - log(y!), s.t. s >= 0
 *     
 * and the Gaussian cavity marginal Q_(s) = N(s; hCav, rhoCav).
 * 
 * 
 * The computation is implemented quadrature free by a recursion with a depth of y, requiring O(y) time and space.
 * 
 * INPUT:
 * - y:   observed count for the current likelihood potential
 * - h:   hCav - rhoCav
 * - rho: rhoCav
 * 
 * OUTPUT:
 * - alpha: d logZ / d hCav
 * - nu   : - d^2 logZ / d hCav^2
 * - logIr: scratch vector of length y+1
 * 
 * RETURNS
 * - logZ 
 * 
 */
double getLogZy2(const int y, const double h, const double rho, double &alpha, double &nu, vec &logIr);

#endif //LRPLOGZ_H
