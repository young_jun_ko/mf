#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_sf_gamma.h>
#include <stdint.h>
#include <iostream>
#include <mex.h>
#include <armadillo>
#include "utils.h"

using arma::vec;
using std::max;
using std::min;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

    const bool FixSize = true;
    const bool NoMemCopy = false;

    if (nrhs != 3) ::mexErrMsgTxt("Expected 3 arguments");
    if (nlhs > 3) ::mexErrMsgTxt("Warning: invalid number of outputs\n");

    const int y = (int)::mxGetPr(prhs[0])[0];
    const double h = ::mxGetPr(prhs[1])[0];
    const double rho = ::mxGetPr(prhs[2])[0];
    mexPrintf("Received Input: y=%d, h=%f, rho=%f\n", y, h, rho);
    plhs[0] = ::mxCreateDoubleMatrix(y + 1, 1, ::mxREAL);

    vec logIr((double *)::mxGetData(plhs[0]), y + 1, NoMemCopy, FixSize);
    logIr.fill(0);

}
