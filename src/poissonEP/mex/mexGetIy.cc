#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_sf_gamma.h>
#include <stdint.h>
#include <iostream>
#include <mex.h>
#include <armadillo>
#include "utils.h"
#include "LRPlogZ.h"

using arma::vec;
using arma::vec;
using std::max;
using std::min;

bool checkLength(const size_t length, const mxArray *input)
{
   return length == ::mxGetNumberOfElements(input);
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

   const bool FixSize = true;
   const bool NoMemCopy = false;

   if (nrhs != 3) ::mexErrMsgTxt("Expected 3 arguments");
   if (nlhs > 3) ::mexErrMsgTxt("Warning: invalid number of outputs\n");

   const size_t nPot = ::mxGetNumberOfElements(prhs[0]);
   if (!checkLength(nPot, prhs[1]) || !checkLength(nPot, prhs[2]))mexErrMsgTxt("Inconsistent Input Dimensions");

   double *yPr = ::mxGetPr(prhs[0]);
   double *hPr = ::mxGetPr(prhs[1]);
   double *rhoPr = ::mxGetPr(prhs[2]);

   plhs[0] = ::mxCreateDoubleMatrix(nPot, 1, ::mxREAL);
   plhs[1] = ::mxCreateDoubleMatrix(nPot, 1, ::mxREAL);
   plhs[2] = ::mxCreateDoubleMatrix(nPot, 1, ::mxREAL);
   vec logZs(::mxGetPr(plhs[0]), nPot, NoMemCopy, FixSize);
   vec dlogZs(::mxGetPr(plhs[1]), nPot, NoMemCopy, FixSize);
   vec d2logZs(::mxGetPr(plhs[2]), nPot, NoMemCopy, FixSize);


   arma::Col<int>  y(nPot);
   for (size_t it = 0; it < nPot; it++) {
      y(it) = (int)yPr[it];
   }

   vec logIr(arma::max(y) + 1);

   vec h(hPr, nPot, NoMemCopy, FixSize);
   vec rho(rhoPr, nPot, NoMemCopy, FixSize);

   for (size_t it = 0; it < nPot; it++) {
      double alpha, nu;
      logZs(it) = getLogZy2(y(it), h(it), rho(it), alpha, nu, logIr);
      dlogZs(it) = alpha;
      d2logZs(it) = -nu;
   }
}

