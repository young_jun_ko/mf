#ifndef UTILS_H
#define UTILS_H

/**
  computes log(a + b) given log(a), log(b)
  */
double logApB(double logA, double logB);

/**
  computes log(a - b) given log(a), log(b)
  */
double logAmB(double logA, double logB);

/**
  evaluates log(N(s | h, rho))
  */
double logGaussPDF(double s, double h, double rho);
#endif
