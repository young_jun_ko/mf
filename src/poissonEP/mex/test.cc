#include <gsl/gsl_sf_log.h>
#include <gsl/gsl_sf_erf.h>
#include <cmath>
#include <iostream>
#include "utils.h"
#include "LRPlogZ.h"

using std::cout;
using std::endl;

int main(int argc, char **argv)
{


   double h = -5;
   double rho = 500.8;
   int y = 1300;
   double t = 1e-5;

   double kappa = -h / sqrt(rho);
   double kappaf = -(h + t) / sqrt(rho);
   double kappab = -(h - t) / sqrt(rho);

   //compute log( 1 - Phi(kappa))

   double logZ0 = ::gsl_sf_log_erfc(kappa / sqrt(2.0)) - log(2.0);

   cout << logZ0 << endl;

   double logZ0f = ::gsl_sf_log_erfc(kappaf / sqrt(2.0)) - log(2.0);
   double logZ0b = ::gsl_sf_log_erfc(kappab / sqrt(2.0)) - log(2.0);

   double dh = (logZ0f - logZ0b) / (2 * t);
   cout << dh << endl;

   //double da = exp(::logGaussPDF(0, h, rho) - ::gsl_sf_log_erfc(kappa))/sqrt(rho);
   double da = ::gsl_sf_hazard(kappa) / sqrt(rho);
   cout << da << endl;
   vec logIr(y + 1);
   double alpha, nu, alphaf, alphab, nuf, nub;

   cout << "START" << endl;
   double logZ = getLogZy(y, h, rho, alpha, nu, logIr);
   exit(0);
   double f = h + t;
   double b = h - t;
   cout << "f,b: " << f << " " << b << endl;
   double logZf = getLogZy(y, f, rho, alphaf, nuf, logIr);
   double logZb = getLogZy(y, b, rho, alphab, nub, logIr);

   dh = (logZf - logZb) / (2 * t);
   cout << "alpha num: " << dh << endl;
   cout << "alpha ana: " << alpha << endl;


   dh = (alphaf - alphab) / (2 * t);
   cout << "nu num: " << dh << endl;
   cout << "nu ana: " << -nu << endl;
   dh = (logZf - 2 * logZ  + logZb) / (t * t);
   cout << "nu num: " << dh << endl;


   cout << "logZ:  " << logZ << endl;
   cout << "logZf: " << logZf << endl;
   cout << "logZb: " << logZb << endl;

//log hazard function
   const double lhk = (::logGaussPDF(0, h, rho) - ::gsl_sf_log_erfc(kappa / sqrt(2.0)) - log(2.0))  + 0.5 * log(rho);

   logIr.zeros();
   logIr(0) = 1;
   logIr(1) = exp(lhk);
   bool isPos = kappa > 0;
   const double lak = log(fabs(kappa));

   for (int r = 2; r <= y; r++) {

      //either kappa is positive or we have a even exponent
      bool posTerm = isPos || ((r - 1) % 2 == 0);
      if (isPos) {

      } else {}

   }


   return 0;
}

