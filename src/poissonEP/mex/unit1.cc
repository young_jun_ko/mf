#include <iostream>
#include <UnitTest++/UnitTest++.h>
#include "LRPlogZ.h"
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_integration.h>

using std::cout;
using std::endl;

struct myFixture {

   myFixture()
   {
   }

   ~myFixture()
   {
   }

   double hCav = 5 ;
   double rhoCav = 1;
   int y = 11;
   double t = 1e-5;

};

void grad(const double t, const int  y, const double h, const double rho, double &alpha, double &nu, vec &logIr, double &logZ, double &dh, double &d2h)
{
   //numerically compute first and second derivative wrt h_ of logZ
   double ap, np;
   double am, nm;

   double hp = h + t;
   double hm = h - t;

   logZ = getLogZy2(y, h, rho, alpha, nu, logIr);
   double logZp = getLogZy2(y, hp, rho, ap, np, logIr);
   double logZm = getLogZy2(y, hm, rho, am, nm, logIr);

   dh  = (logZp - logZm) / (2 * t);
   //its more accurate to differentiate alpha wrt h_ again
   //d2h = (logZp - 2 * logZ + logZm) / (t * t);
   d2h = (ap - am) / (2 * t);
}


TEST_FIXTURE(myFixture, Gradients)
{
   double alpha, nu, dh, d2h, logZ;
   double h = hCav - rhoCav;
   double rho  = rhoCav;

   vec logIr(y + 1);

   grad(t, y, h, rho, alpha, nu, logIr, logZ, dh, d2h);
   cout << "hCav=" << hCav << "; rhoCav" << rhoCav << endl;


   cout << "\t\t\tanalytic\tnumeric" << endl;
   cout << "dlogZ:\t" << alpha << "  " << dh << endl;
   cout << "d2logZ:\t" << -nu << "  " << d2h << endl;
   cout << "logZ=" << logZ <<  " (analytic)"<<endl;
}

struct lrpParams {
   //parameters of the tilted distribution
   const int y;
   const double h;
   const double rho;

   lrpParams(const int _y, const double _h, const double _rho): y(_y), h(_h), rho(_rho) {}
};

double integrand(double s, void *params)
{
    //unnormalized tilted distribution

   const lrpParams *pars = (lrpParams *) params;
   const double y = (double)pars->y;
   const double h = pars->h;
   const double rho = pars->rho;

   const double diff = s - h;
   if (s <= 0)
      return 0;
   else {
      double logP = y * log(s) - s - ::gsl_sf_lngamma(y + 1) - 0.5 * diff * diff / rho - 0.5 * log(2 * M_PI * rho);
      return exp(logP);
   }
}

TEST_FIXTURE(myFixture, SampleLogZ)
{
   const int N = 100000;
   vec samples(N); //samples of the cavity distribution
   samples.randn();
   samples *= sqrt(rhoCav);
   samples += hCav;

   //evaluate log likelihood
   vec logP(N);
   for (int it = 0;  it < N; it++) {
      double s = samples(it);
      if (s < 0) {
         logP(it) = -arma::datum::inf;
      } else {
         logP(it) = y * log(s) - s - ::gsl_sf_lngamma((double)(y + 1));
      }
   }
   double loga = max(logP);

   //aggregate monte carlo estimate
   double logZ = loga + log(sum(exp(logP - loga))) - log((double)N);
   cout << "logZ=" << logZ << " (" << N << " samples)" <<  endl;
}

TEST_FIXTURE(myFixture, QuadLogZ){

   gsl_integration_workspace *w = gsl_integration_workspace_alloc(1000);

   double result, error;
   lrpParams alpha(y, hCav, rhoCav);

   gsl_function F;
   F.function = &integrand;
   F.params = &alpha;

   gsl_integration_qags(&F, 0, 1000, 0, 1e-7, 1000, w, &result, &error);
   cout << "logZ="<<log(result) << " (quadrature)" << endl;

}


int main(int argc, char **argv)
{
   cout << "START" << endl;

   UnitTest::RunAllTests();

   cout << "DONE" << endl;
   return 0;
}



