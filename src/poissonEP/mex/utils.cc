#include <gsl/gsl_sf_log.h>
#include <cmath>
#include <algorithm>
//#include <mex.h>
#include "utils.h"
#include <iostream>

using std::cout;
using std::endl;
double logApB(double logA, double logB)
{
   double la = std::max(logA, logB);
   double lb = std::min(logA, logB);
//   cout << "log1+x " << exp(lb - la) << endl;
   return la + ::gsl_sf_log_1plusx(exp(lb - la));
}

double logAmB(double logA, double logB)
{
   //here we logA > logB
   if (!(logA > logB)) {
       std::cout << "error " << logA - logB << std::endl;
       return log(0);
   }

   //cout << "log1-x " << -exp(logB - logA) << endl;
   //cout << "\t " << logB <<" " << logA << " " << -exp(logA - logB) << " " << -exp(logB - logA) << endl;
   //cout << "a " << logA << " b " << logB<< " e " << exp(logB - logA) << endl;
   return logA + ::gsl_sf_log_1plusx(-exp(logB - logA));
}

double logGaussPDF(double s, double h, double rho)
{
   double diff = s - h;
   return -0.5 * diff * diff / rho - 0.5 * log(2.0 * M_PI * rho);
}
