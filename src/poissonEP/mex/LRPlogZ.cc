#include "LRPlogZ.h"
#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_sf_gamma.h>
#include <cmath>
#include "utils.h"
using std::cout;
using std::endl;

double getLogZy(const int y, const double h, const double rho, double &alpha, double &nu, vec &logIr)
{
   const double kappa = -h / sqrt(rho);
   const double lrho = log(rho);
   const double lah = log(fabs(h));

   const double logI0 = ::gsl_sf_log_erfc(kappa / sqrt(2.0)) - log(2.0);
   const double hk = exp(::logGaussPDF(0, h, rho) - logI0) * sqrt(rho);

   logIr(0) =  logI0;
   if (y > 0) {
      if (h == 0) {

         logIr(1) = lrho + logGaussPDF(0, h, rho);
         for (int r = 2; r <= y; r++) {
            logIr(r) = lrho + log((double)(r - 1)) + logIr(r - 2);
         }

      } else if (h > 0) {

         logIr(1) = logApB(lah + logIr(0), lrho + logGaussPDF(0, h, rho));
         for (int r = 2; r <= y; r++) {
            double lrm1 = log((double)(r - 1));
            logIr(r) = logApB(lah + logIr(r - 1), lrho + lrm1 + logIr(r - 2));
         }

      } else {//h < 0

         logIr(0) = logI0;
         //logIr(1) = logAmB(lrho + logGaussPDF(0, h, rho), lah + logIr(0));

         //cout << lrho << endl;
         //cout << logGaussPDF(0, h, rho) << endl;
         //cout << logIr.t() << endl;
         //   for (int r = 2; r <= y; r++) {
         //      double lrm1 = log((double)(r - 1));
         //      logIr(r) = logAmB(lrho + lrm1 + logIr(r - 2), lah + logIr(r - 1));
         //   cout << "r=" << r << " Lr=" << logIr(r) << endl;
         //   }


         double logZy = logIr(y) - h - 0.5 * rho - ::gsl_sf_lngamma(y + 1);
         //Use the recursive formulation for Lp
         logIr(0) = exp(::logGaussPDF(0, h, rho) - logI0) ;
         for (int r = 1; r <= y; r++) {
            logIr(r) = ((double) r) / (h + rho * logIr(r - 1));
         }
//         cout << logIr.t() << endl;

         double temp = logI0;

         for (int it = 0; it < y; it++) {

            double r = (double)(it + 1);
            //    if(logIr(it+1)<0)
            //        cout << logIr(it+1) <<endl;
            temp += log(r) - log(logIr(it + 1));
         }


         logZy = temp - h - 0.5 * rho - ::gsl_sf_lngamma(y + 1);

         double Lp  = logIr(y)  ;
         double Lpp = -Lp * (Lp - logIr(y - 1));
         cout << Lp << " " << logIr(y - 1) << endl;
         alpha = Lp - 1;
         nu = -Lpp;


         return logZy;

      }
   }
   //std::cout << logIr.t() << std::endl;

   double Lp = 0;
   double Lpp = 0;

   // cout << hk1 << endl;
   //double hk = ::gsl_sf_hazard(kappa);

   if (y == 0) {
      Lp = hk / sqrt(rho);
      Lpp = - Lp * (Lp + h / rho);
      //   cout << Lp << endl;
      //   cout << Lpp << endl;
      return 0;
   } else if (y == 1) {
      double Lp0 = hk / sqrt(rho);
      //Lp = ((double)y) * exp(logIr(y - 1) - logIr(y));
      //std::cout << Lp << std::endl;
      Lp = ((double)y) / (h + rho * Lp0);
      //std::cout << Lp << std::endl;
      Lpp = - Lp * (Lp - Lp0);
   } else {//L>1
      Lp = y * exp(logIr(y - 1) - logIr(y));
      Lpp = y * (y - 1) * exp(logIr(y - 2) - logIr(y)) - Lp * Lp;
   }
   alpha = Lp - 1;
   nu = - Lpp;

   return logIr(y) - h - 0.5 * rho - ::gsl_sf_lngamma(y + 1);
}




double getLogZy2(const int y, const double h, const double rho, double &alpha, double &nu, vec &logIr)
{
   const double kappa = -h / sqrt(rho);

   const double logI0 = ::gsl_sf_log_erfc(kappa / sqrt(2.0)) - log(2.0);
   //double logZy = logIr(y) - h - 0.5 * rho - ::gsl_sf_lngamma(y + 1);
   //Use the recursive formulation for Lp
   logIr(0) = exp(::logGaussPDF(0, h, rho) - logI0) ;
   for (int r = 1; r <= y; r++) {
      logIr(r) = ((double) r) / (h + rho * logIr(r - 1));
   }

   double temp = logI0;

   //Compute Iy from Lp
   for (int it = 0; it < y; it++) {
      double r = (double)(it + 1);
      temp += log(r) - log(logIr(it + 1));
   }

   double logZy = temp - h - 0.5 * rho - ::gsl_sf_lngamma(y + 1);

   double Lp  = logIr(y)  ;
   double Lpp = 0;// -Lp * (Lp - logIr(y - 1));
   if (y > 0)
    Lpp = -Lp * (Lp - logIr(y - 1));
   else
    Lpp = -Lp * (Lp + h / rho);
   //if (Lpp > 0) {
   //   cout << Lp << " " << logIr(y - 1) << endl;
   //   cout << logI0 << endl;
   //   cout << logIr(0) << endl;
   //   logIr(0) = exp(::logGaussPDF(0, h, rho) - logI0) ;
   //   for (int r = 1; r <= y; r++) {
   //      logIr(r) = ((double) r) / (h + rho * logIr(r - 1));
   //      cout << r << ": " << logIr(r) <<" " <<h << " " <<  rho * logIr(r - 1)<< endl;
   //   }
   //}
   alpha = Lp - 1;
   nu = -Lpp;


   return logZy;

}


