hCav = -17;
rhoCav = 13;
y=9;

e=1e-4;

[lp, dlp ,d2lp] = likPoissonLin(y,hCav,rhoCav);

[lpP,dlpP] = likPoissonLin(y,hCav+e,rhoCav);
[lpM,dlpM] = likPoissonLin(y,hCav-e,rhoCav);

g = (lpP - lpM) / (2*e);
g2 = (dlpP - dlpM) / (2*e);
fprintf('############## y = %d\n',y)
fprintf('Analytic: %.16e\n' ,  dlp)
fprintf('Numeric:  %.16e\n' , g )
fprintf('diff:     %.16e\n' , g - dlp)
