close all

addpath(genpath('~/dev/minFunc_2012'))
rng(2);
s2w = 1;
Omega = 1/s2w;
M = 20;
D = 1;

X = 4 +randn(M,D);

w = rand(D,1) * sqrt(s2w);
w = 0.2;


eta = X * w;

y = poissrnd(max(eta,0));

eta
y

fun = @(x) funPoissonTest(x, y,X, s2w);

weval = 0:0.001:0.5;
Z = integral(fun, -Inf, Inf,'waypoints', 0);


pw = fun(weval)/Z;

figure
hold on
plot(weval, pw)
plot(w, max(pw),'rx')


params.prior.mu = 0;
params.prior.Omega = Omega;
Lomega = chol(Omega,'lower');
params.prior.ldOmega = 2 * sum(log(diag(Lomega)));
params.s2l = 1;

postW = [];
[postW] = infGauss(X,y,postW,params);

bla = normpdf(weval, postW.m, sqrt(postW.V));
plot(weval, bla, 'g')

postW.m = 0;
[postW] = infLaplace(X,y,postW,params);

bla = normpdf(weval, postW.m, sqrt(postW.V));
plot(weval, bla, 'c')

