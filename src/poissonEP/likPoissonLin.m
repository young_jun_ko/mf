function [lp, dlp, d2lp] = likPoissonLin(hyp, y, hCav, rhoCav, infer)
    %only for infEP
    rho = rhoCav;
    h = hCav - rhoCav;
    [lp,dlp,d2lp] = mexGetIy(y, h, rho);
end
