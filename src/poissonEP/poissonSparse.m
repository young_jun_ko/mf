close all
clear all
addpath(genpath('~/dev/glm-ie_v1.5'))

u = [0;0;0;0;5;5;5;5;5;2;2;2;2;9;9;9;9;9;0;0;0;0;0];
u = u;
n = length(u);
f = fspecial('gaussian', [5,1],0.7);

s = conv(u,f,'same');

y = poissrnd(s);
sz = size(u);
shape = 'same';
convType = 'conv';


X = matConv(f, sz, shape, convType, []);

s2 = 0.0078;                                                          % variance
B = matFD(sz, shape,[]);  tau = 100;  t = 0;                                  % sparsity transform

% Gaussian or non-Gaussian i.e. sparse weight prior
% select by uncommenting the desired one
 %pot = @(s) potT(s,2);            % Student's t
 pot = @potLaplace;               % Laplace
%pot = @(s) potExpPow(s,1.3);     % Exponential Power family

fprintf('Do sparse regression using a (sparse) '), str = func2str(pot);
fprintf('%s weight prior.\n',str(8:end))

% split into training and test set
%itr = randperm(m); ite = itr(1:end/2); itr = itr(end/2+1:end);

%% Inference
%opts.innerType = 'EP';

opts.innerOutput = 1;
opts.outerOutput = 1;
opts.outerNiter = 3;           % Number of outer loop iterations
opts.outerMethod = 'lanczos';  % Lanczos marginal variance approximation
% opts.outerMethod = 'sample';   % Monte Carlo marginal variance approximation
 opts.outerMethod = 'full';     % Exact marginal variance computation (slow)
% opts.outerMethod = 'woodbury'; % Exact marginal variance computation (fast)

opts.innerMVM =  40;           % number CG steps
opts.innerVBpls = 'plsCG';     % PLS algorithm, also LBFGS if compiled
opts.innerExact = 1;
%[m,ga,b,z,zu,nlZ,Q,T] = dli(X(itr,:),y(itr),s2,B,t,pot,tau,opts);
[m,ga,b,z,zu,nlZ,Q,T] = dli(X,y,s2,B,t,pot,tau,opts);

% posterior quantities:
% m = E(u) posterior mean estimate
% z  = var(B*u) marginal variance estimate
% nlZ sequence of negative log marginal likelihoods, nlZ(end) is the last one
% zu = var(u) marginal variance estimate
% Q,T yield cov(u) = V = inv(A) \approx Q'*inv(T)*Q the posterior covariance

%% Estimation
opt.nMVM = 50; opt.output = 1; opt.exactNewt = 1;
uhat = feval(opts.innerVBpls,zeros(n,1),X,y,B,t,opt,s2,'penVB',pot,tau);

figure
hold on

plot(u, 'rx-')
plot(uhat, 'bx-')
plot(m, 'gx-')

