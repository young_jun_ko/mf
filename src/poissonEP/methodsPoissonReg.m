function [postW] = methodsPoissonReg(name, X,y,postW, params)

    switch name
        case 'mcmc'
        case 'gauss'
        case 'vbExp'
        case 'epLin'
        case 'epExp'
        case 'quad1d'
        case 'lapLin'
        otherwise
            error('Invalid Methods Name')
    end

end
