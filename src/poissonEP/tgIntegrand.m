function y = tgIntegrand(n,h,rho,s)
    % evaluates normpdf(s, h, sqrt(rho)) * s^n for s>=0
    if s < 0
        y=0;
    else
        %y = s.^n .* normpdf((s-h)./sqrt(rho));
        y = n .* log(s) - 0.5 * (s-h).^2 / rho - 0.5 * log(2*pi*rho);
        y =exp(y);
    end

end

