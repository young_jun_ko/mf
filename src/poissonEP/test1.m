function test1
h = 10;
rho = 100000008;
rho = 1e5;
h = h - rho;
y = 1;

f = @(x) tgIntegrand(y, h, rho, x);
a = integral(f,0,inf) 

fprintf('Res. Integration: %.16f\n', log(a))

t=1e-6;
logZ = getLogZ(y, h, rho)
logZf = getLogZ(y, h+t, rho);
logZb = getLogZ(y, h-t, rho);

dh = (logZf - logZb) / (2*t);
dh

dh = (logZf - 2*logZ+ logZb) / (t*t);
dh

hyp = [];
Y = [y;y];
hCav = [h + rho;h+rho];
rhoCav = [rho;rho];
infer = 'infEP';
[lp, dlp, d2lp] = likPoissonLin(hyp, Y, hCav, rhoCav, infer)

end

function logZ = getLogZ(y,h,rho)

f = @(x) tgIntegrand(y, h, rho, x);
Iy = integral(f,0,inf) ;
logZ = log(Iy) - h - 0.5 * rho - sum(log(1:y)) ;

end
