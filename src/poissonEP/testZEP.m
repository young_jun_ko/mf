rng(123);
D=5;
M = 10;

X = randn(M,D);

Pi = diag(rand(M,1)*0.1);
St = diag(1./ diag(Pi));
beta = randn(M,1);

Omega = randn(D);
Omega = Omega'*Omega;
Lo = chol(Omega,'lower');

A = Omega + X'* Pi * X;
Ls = chol(A,'lower');

Sigma = Ls' \ (Ls \ eye(D));

K = Lo \ X';
K = K' * K;

Lk = chol(K + St,'lower');

ldk = 2 * sum(log(diag(Lk)));

lds = -sum(log(diag(Pi))) -2* sum(log(diag(Lo))) + 2* sum(log(diag(Ls)));

ldk
lds

tmu = St * beta;
temp = Lk \ tmu;
dot(temp, temp)

mu = Sigma * X' * beta;
dot(beta, St * beta) - dot(mu, A * mu)
