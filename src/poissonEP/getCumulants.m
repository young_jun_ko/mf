function C = getCumulants(M)
    % recursion: Moments -> Cumulants from wikipedia

    C = zeros(size(M));

    n = size(M,2); % number of moments is number of cumulants

    C(:,1) = M(:,1); %base case: kappa1 = mu1
    kernel = [1,1];

    prow = 1;

    for it = 2:n

        prow = conv(prow, kernel); %it-th row of pascals triangle

        temp = zeros(size(C,1), 1);
        for mt = 1:(it-1)
            temp = temp + prow(mt) * C(:,mt) .* M(:,it- mt);
        end
        C(:,it) = M(:,it) - temp;
    end
end
