function [corr] = getMeanCorr(post, y,K,order, knots, gridE)


    v = diag(post.Sigma);
    s = 1./v;
    ttau = post.ttau;
    tnu = post.tnu;
    tau_n = s - ttau;
    nu_n = post.mu .* s - tnu;

    hCav = nu_n./tau_n;
    rhoCav = 1./tau_n;
    h = hCav - rhoCav;
    rho = rhoCav;

    [lZ, dlZ, d2lZ] = mexGetIy(y, h, rho);
    nMom = 20;
    logMom = zeros(length(lZ), nMom);

    for m = 1:nMom
        [mom] = mexGetIy(y+m, h, rho);
        corry = bsxfun(@plus, y, 1:m);
        logMom(:, m) = sum(log(corry),2) + mom - lZ;
    end
    M = exp(logMom);
    m1 = M(:,1);
    m2 = M(:,2);
    m3 = M(:,3);
    m4 = M(:,4);
    m5 = M(:,5);
    m6 = M(:,6);
    c3 = m3 - 3 * m2.* m1 + 2* (m1.^3);
    c4 = m4 - 4 * m3 .* m1 - 3 * m2.^2 + 12 * m2.* m1.^2 - 6 * m1.^4;
    c5 = m5 - 5 * m4 .* m1 - 10 * m3.*m2 + 20 * m3.*m1.^2 + 30 * m2.^2.*m1-60*m2.*m1.^3 + 24 * m1.^5; 
    c6 = m6 - 6*m5.*m1-15*m4.*m2+30*m4.*m1.^2-10*m3.^2+120*m1.*m2.*m3-120*m3.*m1.^3+30*m2.^3-270*(m1.*m2).^2+360*m2.*m1.^4-120*m1.^6;
    C = [c3, c4,c5,c6];

    C = getCumulants(M);

    B = bspline_basismatrix(order, knots, gridE);
    Ball = [K; B];
    SigmaAll = Ball * post.SigmaB * Ball';

    nTest = length(gridE);
    nTrain = size(K,1);
    L = 4;
    Corr = zeros(nTest, L);
    for it = 1:nTest

        for lt = 3:L
            lIdx = lt-2;
            lfact = prod(1:lt);
            for jt = 1:nTrain
                for nt = 1:nTrain
                    if jt ~= nt
                        covFact = (SigmaAll(jt,nt)/ (SigmaAll(jt,jt) * SigmaAll(nt,nt)))^lt;
                        Corr(it,lIdx) = Corr(it,lIdx) +covFact* SigmaAll(it+nTrain, jt) * C(jt, lIdx+1) * C(nt, lIdx) / (SigmaAll(jt,jt) *lfact) ; 
                    end
                end
            end
        end

    end

    corr = sum(Corr,2);

end
