function truncTest()
    h = 10;
    rho = 3;
    y = 0;

    f = @(x) Eqn(y, h, rho, x);
    Z0 = (0.5 * erfc(-h/sqrt(rho * 2)))

    a = integral(f,0,inf) ;
    %fprintf('Res. Integration: %.16f\n', a)
    fprintf('Res. Integration(log): %.16f\n', log(a))
    %b = getlz(y,h,rho);
    %fprintf('Res. Closed Form: %.16f\n', b)
    %fprintf('Res. Closed Form(log): %.16f\n', log(b))
    %c = getI(y,h,rho);
    %fprintf('Res. Closed Form: %.16f\n', c(end))
    %fprintf('Res. Closed Form(log): %.16f\n', log(c(end)))
    %logd = getLogI(y,h,rho);
    %d = exp(logd);
    %fprintf('Res. Closed Form: %.16f\n', d(end))
    %fprintf('Res. Closed Form(log): %.16f\n', logd(end))

    %fprintf('reldiff %.16e\n', (log(a) - (logd(end)))/max(log(a), logd(end)))

    [lp,dlp,d2lp] = mexGetIy(y, h-rho, rho)

function y = Eqn(n,h,rho,s)
    % evaluates normpdf(s, h, sqrt(rho)) * s^n for s>=0
    if s <= 0
        y=0;
    else
        %y = s.^n .* normpdf((s-h)./sqrt(rho));
        y = n .* log(s) - s - sum(log(1:n))   - 0.5 * (s-h).^2 / rho - 0.5 * log(2*pi*rho);
        y = exp(y);
    end


function prow = pascal(y)
    %computes a row of pascals triangle, i.e. nchoosek(y,r) for y = 0:y
    prow = zeros(y+1,1);
    prow(1) = 1;
    for r = 1:y
        prow(r+1) = prow(r) * (y + 1 - r);
    end
    prow(3:end) = prow(3:end) ./ (2:y)';

function pow = powers(y, h)
    %calculates integer powers h^r for r = 0:y
    pow = zeros(y+1,1);
    pow(1) = 1;
    for r = 1:y
        pow(r+1) = pow(r) * h;
    end


function [logZ] = getlz(y, h,rho)
    %TODO
    % - precompute nchoosek for r = 0:y (or 0:y/2 ?)
    % - precompute h^r for r = 0:y

    u = -h / sqrt(rho);
    % the sum has y+1 terms, index r in [0,y]
    Ir = zeros(y+1,1);
    Ir(1) = 1;%r = 0
    hu = hazard(u);
    Ir(2) = hu;%r = 1
    ur = u;% for r = 2 we need u^2
    % add the first two terms r = 0,1
    logZ = h^y*Ir(1) + y*h^(y-1)*sqrt(rho)*Ir(2);
    for r=2:1:y
        Ir(r+1) = ur * hu + (r-1) * Ir(r-2 + 1);
        ur = ur*u;
        logZ = logZ + nchoosek(y,r) * h^(y-r) * sqrt(rho^(r)) * Ir(r+1);
    %    logZ
    end

function Iy = getI(y,h,rho)
    kappa = - h / sqrt(rho);
    hk = hazard(kappa);

    Iy = zeros(y+1,1);

    Iy(1) = 1;
    Iy(2) = h + sqrt(rho) * hk;

    for r = 2:1:y
        Iy(r+1) = h * Iy(r) + (r-1) * rho * Iy(r-1);
    %    Iy(r+1)
    end

function logIy = getLogI(y,h,rho)
    logh = log(abs(h));
    logrho = log(rho);
    kappa = - h / sqrt(rho);
    hk = hazard(kappa);

    logIy = zeros(y+1,1);

    logIy(1) = 0;
    logIy(2) = log(h + sqrt(rho)*hk);

    if h == 0 
        for r = 2:1:y
            logIy(r+1) = log(r-1) + logrho + logIy(r-1);
        end
    else
        if h < 0
            %we have h < 0 so (r-1) * rho * Iy(r-1) - abs(h) * Iy(r)
            %we have to assume that the first term is larger 
            for r = 2:1:y
                loga = log(r-1) + logrho + logIy(r-1);
                logb = logh + logIy(r);
                logIy(r+1) = loga  + log1p(- exp(logb - loga));
            end
        else
            %we have h > 0
            %we need to check which term is larger
            for r = 2:1:y
                loga = log(r-1) + logrho + logIy(r-1);
                logb = logh + logIy(r);
                if loga > logb
                    logIy(r+1) = loga + log1p(exp(logb - loga));
                else
                    logIy(r+1) = logb + log1p(exp(loga - logb));
                end
                % fprintf('%f\n', logIy(r+1) )
            end
        end
    end
