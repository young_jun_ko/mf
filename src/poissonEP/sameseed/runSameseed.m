% Sets up the Gaussian mixture model test case of Section 5 of 
% http://arxiv.org/pdf/1401.8274v1.pdf For demonstration purposes these
% data are then unfolded using Tikhonov regularization. Also an MCMC
% sample from the posterior is provided.
%
% The most important outputs are:
%
% y       smeared data on a histogram with nBinsF bins on interval [lbF,ubF]
% K       smearing matrix corresponding to convolving with a Gaussian
% f       intensity function of the true spectrum evaluated on gridE
% g       intensity function of the smeared spectrum evaluated on gridF
% omega   precision matrix of the Gaussian smoothness prior
% omegaA  as above but with the Aristotelian boundary conditions
%
%
% Author: Mikael Kuusela (e-mail: mikael.kuusela@epfl.ch)

rng(2014);

clear;
close all;

load gmmSetup20kSameSeed.mat
%{  
nBinsF = 40; % Number of bins for the smeared histogram
lbF = -7; % Lower bound of the smeared histogram
ubF = 7; % Upper bound of the smeared histogram
binsF = linspace(lbF,ubF,nBinsF+1); % Binning of the smeared histogram

lbE = -7; % Lower bound of the true space
ubE = 7; % Upper bound of the true space
order = 4; % Cubic splines
nKnots = 26; % Number of knots for the spline expansion
knots = linspace(lbE,ubE,nKnots+2);
knots = augknt(knots,order);
p = nKnots + order;

% Set up evaluation grids for the intensity functions
nGridE = 500;
gridE = linspace(lbE,ubE,nGridE);
nGridF = 500;
gridF = linspace(lbF,ubF,nGridF);

lambdaTot = 20000; % Expected total number of observations

% Parameters of the Gaussian mixture model
pi1 = 0.2;
mu1 = -2;
sigma1 = 1;
pi2 = 0.5;
mu2 = 2;
sigma2 = 1;
pi3 = 0.3;

sigmaConv = 1; % Standard deviation of the convolving Gaussian

% Generate the smeared data
N = poissrnd(lambdaTot);
Ncomp = mnrnd(N,[pi1 pi2 pi3]);
xPoint = [normrnd(mu1,sigma1,1,Ncomp(1)) normrnd(mu2,sigma2,1,Ncomp(2)) unifrnd(lbE,ubE,1,Ncomp(3))];
yPoint = xPoint + normrnd(0,sigmaConv,1,N);
y = histc(yPoint,binsF)';
y = y(1:end-1);

% Construct smearing matrix K
K = zeros(nBinsF,p);
for i=1:nBinsF
    ki = @(x)(normcdf(binsF(i+1),x,sigmaConv) - normcdf(binsF(i),x,sigmaConv));
    for j=1:p
        beta = zeros(1,p);
        beta(j) = 1;
        Bj = spmak(knots,beta);
        fun = @(x)(ki(x).*fnval(Bj,x));
        intLb = knots(j);
        intUb = knots(j+order); % NB: need to check that this works when order != 4
        K(i,j) = quadgk(fun,intLb,intUb);
    end
end

% Store true intensity f
fFun = @(s) gmm(s,pi1,mu1,sigma1,pi2,mu2,sigma2) + pi3*1/(ubE-lbE);
f = lambdaTot*fFun(gridE);

% Store smeared intensity g
g = zeros(1,nGridF);
for i=1:nGridF
    t = gridF(i);
    integrand = @(s) normpdf(t,s,sigmaConv).*fFun(s);
    g(i) = lambdaTot*quadgk(integrand,lbE,ubE);
end

% Construct prior precision omega
omega = zeros(p,p);
for i=1:p
    beta1 = zeros(1,p);
    beta1(i) = 1;
    Bi = spmak(knots,beta1);
    D2Bi = fnder(Bi,2);
    for j=1:p
        beta2 = zeros(1,p);
        beta2(j) = 1;
        Bj = spmak(knots,beta2);
        D2Bj = fnder(Bj,2);
        D2BiD2Bj = fncmb(D2Bi,'*',D2Bj);
        omega(i,j) = diff(fnval(fnint(D2BiD2Bj),[lbE ubE]));
    end
end

save gmmSetup20kSameSeed.mat
return
%}
% Construct augmented omega (Aristotelian boundary conditions)
gammaLeft = 5;
gammaRight = 5;
omegaA = omega;
omegaA(1,1) = omegaA(1,1) + gammaLeft;
omegaA(end,end) = omegaA(end,end) + gammaRight;

% Tikhonov regularization (i.e. MAP with Gaussian likelihood and no positivity constraint)
covInv = diag(y.^-1);
delta = 2.5e-7;
betaHatTik = (K'*covInv*K + delta*omegaA)\(K'*covInv*y);

hyp.delta = 2*delta;
hyp.omegaA = omegaA;
hyp.lik = [];
lik = { @likPoissonLin };
del = 5e-7;
%[post,nlZ,dnlZ] = runPoissonEP(hyp, [], [], lik, K, y , betaHatTik);
for d = 1:1
    %hyp.delta = Deltas(d);
    hyp.delta = del;
    tic;
    [post,nlZ,dnlZ] = runPoissonEP(hyp, [], [], lik, K, y , betaHatTik);
    toc
    Ebb = post.muB * post.muB' + post.SigmaB;
    del = p/ (trace( Ebb * omegaA));
    pause

end

% Load MCMC sample from the posterior and compute the posterior mean
load('betaSample1000.mat');
betaHatPostMean = mean(betaSample,2);

% Construct the estimated intensity
fHatTik = spmak(knots,betaHatTik');
fHatTikGridE = fnval(fHatTik,gridE);
fHatPostMean = spmak(knots,betaHatPostMean');
fHatPostMeanGridE = fnval(fHatPostMean,gridE);

fHatEP = spmak(knots,post.muB');
fHatEPGridE = fnval(fHatEP,gridE);
% Plot intensities
figure;
hold on;
plot(gridE,fHatTikGridE,'b');
plot(gridE,fHatEPGridE,'g');
plot(gridE,fHatPostMeanGridE,'r');
plot(gridE,f,'k');
plot(gridF,g,'m');
hold off;
ylim([0,1.1*max(f)]);
xlim([lbE ubE]);
box on;
leg = legend('Tikhonov','EP','Posterior mean','True intensity','Smeared intensity','Location','Northwest');
set(leg,'FontSize',9);
ylabel('Intensity');
