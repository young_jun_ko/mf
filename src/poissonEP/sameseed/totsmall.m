rng(2014);

clear;
close all;

ltots = [50,100,150,200,250,500];
ltots = [150];
binss = [50];

for l = 1:length(ltots)
    lam = ltots(l);

    for b = 1:length(binss)
        bins = binss(b);
        params.nBinsF = bins; % Number of bins for the smeared histogram
        params.lbF = -7; % Lower bound of the smeared histogram
        params.ubF = 7; % Upper bound of the smeared histogram
        params.lbE = -7; % Lower bound of the true space
        params.ubE = 7; % Upper bound of the true space
        params.order = 4; % Cubic splines
        params.nKnots = 26; % Number of knots for the spline expansion
        params.nGridE = 500;
        params.nGridF = 500;
        params.lambdaTot = lam; % Expected total number of observations
        params.pi1 = 0.2;
        params.mu1 = -2;
        params.sigma1 = 1;
        params.pi2 = 0.5;
        params.mu2 = 2;
        params.sigma2 = 1;
        params.pi3 = 0.3;
        params.sigmaConv = 1; % Standard deviation of the convolving Gaussian
       params.gammaLeft = 1.5;
        params.gammaRight = 1.5;

        params.seed = 2014;
        [y, K, f, g, omega, omegaA,p, knots, gridE, gridF] = genDataUnfolding(params);
%        y=y+1;
        % Tikhonov regularization (i.e. MAP with Gaussian likelihood and no positivity constraint)
        covInv = diag(y.^-1);
        covInv =  eye(length(y));
        delta = 3e-3;
        betaHatTik = (K'*covInv*K + delta*omegaA)\(K'*covInv*y);

        hyp.delta = 2*delta;
        hyp.omegaA = omegaA;
        hyp.lik = [];
        lik = { @likPoissonLin };
        del = 1e-5;
        del =  2 * delta;
        del = 1e-4;
        %[post,nlZ,dnlZ] = runPoissonEP(hyp, [], [], lik, K, y , betaHatTik);
        figure
        hold on
        for d = 1:1
            %hyp.delta = Deltas(d);
            hyp.delta = del;
            tic;
            [post,nlZ,dnlZ] = runPoissonEP(hyp, [], [], lik, K, y , betaHatTik);
            toc
            Ebb = post.muB * post.muB' + post.SigmaB;
            del = p/ (trace( Ebb * omegaA));
            fprintf('deltaEP: %f\n',del)
            plot(d, del, 'rx')

 [corr] = getMeanCorr(post, y,K,params.order, knots, gridE);
norm( corr )
%figure, plot(corr)
        end

        % Load MCMC sample from the posterior and compute the posterior mean
        load('betaSample1000.mat');
        betaHatPostMean = mean(betaSample,2);

        % Construct the estimated intensity
        fHatTik = spmak(knots,betaHatTik');
        fHatTikGridE = fnval(fHatTik,gridE);
        fHatPostMean = spmak(knots,betaHatPostMean');
        fHatPostMeanGridE = fnval(fHatPostMean,gridE);

        fHatEP = spmak(knots,post.muB');
        fHatEPGridE = fnval(fHatEP,gridE);
        % Plot intensities
        figure;
        hold on;
        plot(gridE,fHatTikGridE,'b');
        plot(gridE,fHatEPGridE,'g');
        plot(gridE,fHatPostMeanGridE,'r');
        plot(gridE,f,'k');
        plot(gridF,g,'m');
        hold off;
        ylim([0,1.1*max(f)]);
        xlim([params.lbE params.ubE]);
        box on;
        leg = legend('Tikhonov','EP','Posterior mean','True intensity','Smeared intensity','Location','Northwest');
        set(leg,'FontSize',9);
        ylabel('Intensity');
    end
end
