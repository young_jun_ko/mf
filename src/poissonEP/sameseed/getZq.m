function logZq = getZq(ttau, tnu, mu, A, Ls, Lo)
    % ttau, tnu: EP parameters
    % mu: posterior mean
    % A = Ls * Ls': posterior precision (and cholesky)
    % Omega = Lo * Lo': prior precision

    %log | K + tSigma |
    ldTerm = -sum(log(ttau)) -2* sum(log(diag(Lo))) + 2* sum(log(diag(Ls)));

    %tmu'*inv(K + tSigma)*tmu
    sqTerm = dot(tnu, tnu ./ ttau) - dot(mu, A * mu);

    logZq = - 0.5 * (ldTerm + sqTerm);

end
