function [post,nlZ,dnlZ] = runPoissonEPSeq(hyp, mean, cov, lik, x, y, betaHat)

    % Expectation Propagation approximation to the posterior Gaussian Process.
    % The function takes a specified covariance function (see covFunction.m) and
    % likelihood function (see likFunction.m), and is designed to be used with
    % gp.m. See also infFunctions.m. In the EP algorithm, the sites are 
    % updated in random order, for better performance when cases are ordered
    % according to the targets.
    %
    % Copyright (c) by Carl Edward Rasmussen and Hannes Nickisch 2013-09-13.
    %
    % See also INFMETHODS.M.

    persistent last_ttau last_tnu              % keep tilde parameters between calls
    tol = 1e-4; max_sweep = 200; min_sweep = 2;     % tolerance to stop EP iterations

    infer = 'infEP';
    n = size(x,1);

    % A note on naming: variables are given short but descriptive names in 
    % accordance with Rasmussen & Williams "GPs for Machine Learning" (2006): mu
    % and s2 are mean and variance, nu and tau are natural parameters. A leading t
    % means tilde, a subscript _ni means "not i" (for cavity parameters), or _n
    % for a vector of cavity parameters. N(f|mu,Sigma) is the posterior.

    % marginal likelihood for ttau = tnu = zeros(n,1); equals n*log(2) for likCum*
    %nlZ0 = -sum(feval(lik{:}, hyp.lik, y, m, diag(K), inf));
    ttau = zeros(n,1); tnu  = zeros(n,1);        % init to zero if no better guess
    Aprior = hyp.delta * hyp.omegaA;
    L = chol(Aprior,'lower');
    ldOmega = 2 * sum(log(diag(L)));
    temp = L * x';
    K = temp' * temp;
    Sigma = K +eye(size(K));                     % initialize Sigma and mu, the parameters of ..
    mu = ones(n,1); 
    m = zeros(n,1); 
    muB = zeros(size(x,2),1);
    mu = x * betaHat;
    nlZ = 0; 
    dnlZ = 0; 

    for it = 1:60
        ridx = randperm(n);
        for jt = 1:n
            j = ridx(jt);
            v = diag(Sigma);
            s = 1./v(j);
            tau_n = s - ttau(j);
            nu_n = mu(j) .* s - tnu(j);

            hCav = nu_n./tau_n;
            rhoCav = 1./tau_n;
            h = hCav - rhoCav;
            rho = rhoCav;
            % compute the desired derivatives of the indivdual log partition function
            h'
            rho'
            % pause
            [lZ, dlZ, d2lZ] = mexGetIy(y(j), h, rho);

            %update EP parameters to match moments
            ttau(j) = -d2lZ  ./(1+d2lZ./tau_n);
            ttau(j) = max(ttau(j),1e-5); % enforce positivity i.e. lower bound ttau by zero
            tnu(j)  = (dlZ - nu_n./tau_n.*d2lZ )./(1+d2lZ./tau_n);

            format long
            ttau'
            %for checking expectation consistency calculate:
            hEC = hCav(:) + dlZ .* rhoCav(:);
            rhoEC = rhoCav(:) .* (1 + d2lZ .* rhoCav(:));
tauOld = ttau;
        B1 = x' * diag(tauOld) * x + Aprior;
        L1 = chol(B1,'lower');
        temp1 = L1 \ x';
        S1 = temp1' *temp1;

        [v(j), rhoCav, rhoEC, S1(j)]
        pause
            %After EP update: logZep
            %   the EP approximation to logZ is:
            %       sum log Zhat + log int prod N(f | tilde mu, tilde sigma) N(w | 0, inv Omega) dw
            tau_n = 1./diag(Sigma)-ttau;             % compute the log marginal likelihood
            nu_n  = mu./diag(Sigma)-tnu;                    % vectors of cavity parameters
            p = tnu-m.*ttau; q = nu_n-m.*tau_n; 
            ldSigmaB = -2 * sum(log(diag(L)));
            %this part comes from the integral of the product of Gaussians
            lZg = 0.5 * (ldSigmaB + muB' * x' * tnu - dot(tnu, ttau .* tnu) + ldOmega - sum(log(ttau)));
            %to that we add the part coming from the site partition functions
            nlZ = - lZg ...
                -sum(lZ) -sum( (hCav - tnu./ttau).^2 ./ (rhoCav+1./tau_n) )/2 - sum(log(rhoCav+1./tau_n))/2;

            fprintf('EC: %f %f nlZ: %f\n ' , norm(mu - hEC, inf), norm(v - rhoEC,inf), nlZ)

            %compute posterior 
            A = x' * diag(ttau) * x + Aprior;
            L = chol(A,'lower');
            muB = L' \ (L \ (x' * tnu));
            SigmaB = L' \ (L \ eye(length(muB)));
            %f marginals
            temp = L \ x';
            Sigma = temp' * temp;
            mu = x * muB;
            %it
        end 
    end
    post.muB = muB;
    post.SigmaB = SigmaB;
    return

    if sweep == max_sweep && abs(nlZ-nlZ_old) > tol
        error('maximum number of sweeps exceeded in function infEP')
    end

    last_ttau = ttau; last_tnu = tnu;                       % remember for next call
    post.alpha = alpha; post.sW = sqrt(ttau); post.L = L;  % return posterior params
    post.Sigma = Sigma;
    post.mu = mu;
    post.ttau = ttau;
    post.tnu = tnu;

    if nargout>2                                           % do we want derivatives?
        dnlZ = hyp;                                   % allocate space for derivatives
        tau_n = 1./diag(Sigma)-ttau;             % compute the log marginal likelihood
        nu_n  = mu./diag(Sigma)-tnu;                    % vectors of cavity parameters
        sW = sqrt(ttau);
        F = alpha*alpha'-repmat(sW,1,n).*solve_chol(L,diag(sW));   % covariance hypers
        for i=1:length(hyp.cov)
            dK = feval(cov{:}, hyp.cov, x, [], i);
            dnlZ.cov(i) = -sum(sum(F.*dK))/2;
        end
        for i = 1:numel(hyp.lik)                                   % likelihood hypers
            dlik = feval(lik{:}, hyp.lik, y, nu_n./tau_n, 1./tau_n, inf, i);
            dnlZ.lik(i) = -sum(dlik);
        end
        [junk,dlZ] = feval(lik{:}, hyp.lik, y, nu_n./tau_n, 1./tau_n, inf);% mean hyps
        for i = 1:numel(hyp.mean)
            dm = feval(mean{:}, hyp.mean, x, i);
            dnlZ.mean(i) = -dlZ'*dm;
        end
    end

    % function to compute the parameters of the Gaussian approximation, Sigma and
    % mu, and the negative log marginal likelihood, nlZ, from the current site
    % parameters, ttau and tnu. Also returns L (useful for predictions).
function [Sigma,mu,L,alpha,nlZ] = epComputeParams(K,y,ttau,tnu,lik,hyp,m,inf)
    n = length(y);                                      % number of training cases
    sW = sqrt(ttau);                                        % compute Sigma and mu
    L = chol(eye(n)+sW*sW'.*K);                            % L'*L=B=eye(n)+sW*K*sW
    V = L'\(repmat(sW,1,n).*K);
    Sigma = K - V'*V;
    alpha = tnu-sW.*solve_chol(L,sW.*(K*tnu+m));
    mu = K*alpha+m; v = diag(Sigma);

    tau_n = 1./diag(Sigma)-ttau;             % compute the log marginal likelihood
    nu_n  = mu./diag(Sigma)-tnu;                    % vectors of cavity parameters
    lZ = feval(lik{:}, hyp.lik, y, nu_n./tau_n, 1./tau_n, inf);
    p = tnu-m.*ttau; q = nu_n-m.*tau_n;                        % auxiliary vectors
    nlZ = sum(log(diag(L))) - sum(lZ) - p'*Sigma*p/2 + (v'*p.^2)/2 ...
        - q'*((ttau./tau_n.*q-2*p).*v)/2 - sum(log(1+ttau./tau_n))/2;
