x = 0:25;


lambdas = 1:10;

figure
hold on
for it = 1:length(lambdas)
    
    lambda = lambdas(it);
    plot(x, poisspdf(x, lambda), 'b');
    plot(x, normpdf(x, lambda, sqrt(lambda)), 'r');

end
