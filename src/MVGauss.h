
#ifndef MVGAUSS_H_
#define MVGAUSS_H_
#include <armadillo>

using arma::mat;
using arma::vec;
using arma::uword;

class MVGauss{

    protected:
        mat Sigma;
        mat L;
        vec mu;
        uword D;
        double ldSigma;
    public:
        MVGauss(){}
        virtual ~MVGauss(){}

        static void nat2mom(const mat & A, const vec & b, mat & L, mat & Sigma, vec & mu, double & ldSigma){}


};



#endif
